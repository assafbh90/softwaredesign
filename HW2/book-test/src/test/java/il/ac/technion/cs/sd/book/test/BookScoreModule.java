package il.ac.technion.cs.sd.book.test;

import java.util.Optional;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import il.ac.technion.cs.sd.book.app.*;
import il.ac.technion.cs.sd.lib.Database;
import il.ac.technion.cs.sd.lib.DatabaseBuilder;
import il.ac.technion.cs.sd.lib.DatabaseBuilderDefault;
import il.ac.technion.cs.sd.lib.DatabaseModule;
import il.ac.technion.cs.sd.lib.LibraryManager;
import il.ac.technion.cs.sd.lib.LibraryManagerImpl;

public class BookScoreModule extends AbstractModule {
	static public Injector getInjector() {
		return Guice.createInjector(new BookScoreModule(),new DatabaseModule());
	}
	@Override
	protected void configure() {
		bind(BookScoreInitializer.class).to(BookScoreInitializerImpl.class);
		bind(BookScoreReader.class).to(BookScoreReaderImpl.class);
		bind(LibraryManager.class).toInstance(DatabaseModule.getInjector().getInstance(LibraryManager.class));
	}
}
