package il.ac.technion.cs.sd.book.test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Scanner;

import org.hamcrest.number.IsCloseTo;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import com.google.inject.Guice;
import com.google.inject.Injector;

import il.ac.technion.cs.sd.book.app.BookScoreInitializer;
import il.ac.technion.cs.sd.book.app.BookScoreReader;
import il.ac.technion.cs.sd.book.ext.LineStorageModule;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class DataIntegration {

  @Rule public Timeout globalTimeout = Timeout.seconds(30);

  private static BookScoreReader setupAndGetReader(String fileName) throws Exception {
    String fileContents =
        new Scanner(new File(DataIntegration.class.getResource(fileName).getFile())).useDelimiter("\\Z").next();
    Injector injector = Guice.createInjector(new BookScoreModule(), new LineStorageModule());
    injector.getInstance(BookScoreInitializer.class).setup(fileContents);
    return injector.getInstance(BookScoreReader.class);
  }

  @Test
  public void getReviewersCheck() throws Exception {
    BookScoreReader reader = setupAndGetReader("small.xml");
    assertEquals(reader.getReviewers("Foobar").size(), 1);
    assertEquals(reader.getReviewers("Foobar").get(0), "123");
    assertTrue(reader.getReviewers("NOT EXISTS").isEmpty());
    assertThat(reader.getAverageReviewScoreForBook("Foobar").getAsDouble(), closeTo(10.0, 0.1));
    assertThat(reader.getAverageReviewScoreForBook("Moobar").getAsDouble(), closeTo(3.0, 0.1));
  }
  
  @Test
  public void mediumCheckReviewers() throws Exception {
    BookScoreReader reader = setupAndGetReader("medium.xml");
    List<String> l = reader.getReviewers("HjEwDQx");
    assertTrue(l.contains("CUJ3qA2V"));
    assertEquals(l.size(), 2);
    assertTrue(reader.getReviewsForBook("HjEwDQx").containsKey("CUJ3qA2V"));
    assertFalse(reader.getReviewsForBook("HjEwDQx").containsKey("NOT EXISTS"));
    assertThat(reader.getAverageReviewScoreForBook("HjEwDQx").getAsDouble(), closeTo(9.0, 0.1));
    assertTrue(reader.gaveReview("CUJ3qA2V", "HjEwDQx"));
  }
  
  @Test
  public void mediumCheckCollectionSizeEquality() throws Exception {
    BookScoreReader reader = setupAndGetReader("medium.xml");
    List<String> l = reader.getReviewers("XRG");
    assertEquals(8, l.size());
    
    assertEquals(reader.getReviewsForBook("XRG").size(), 
    			 reader.getReviewers("XRG").size());
    
    assertTrue(reader.gaveReview("GUnLxZI9", "XRG"));
    assertThat(reader.getAverageReviewScoreForBook("XRG").getAsDouble(), closeTo(3.375, 0.1));
  }
}
