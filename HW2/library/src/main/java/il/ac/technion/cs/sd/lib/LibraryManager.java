package il.ac.technion.cs.sd.lib;

import java.util.Optional;

public interface LibraryManager {
	DatabaseBuilder Builder();
	Optional<Database> getDatabase();
	void setDatabase(Database database);
}
