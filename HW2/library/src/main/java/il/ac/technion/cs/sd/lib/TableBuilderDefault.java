package il.ac.technion.cs.sd.lib;

import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

class TableBuilderDefault<T> implements TableBuilder<T> {
	final private Map<String, Table<?>> tables;
	final private String name;
	final private Serializer<T> serializer;
	@Inject
	private DatabaseBuilderFactory dbBuilderFactory;
	@Inject
	private TableFactory tableFactory;
	@Inject
	private StorageFactory storageFactory;

	TableBuilderDefault(Map<String, Table<?>> tables, String name, Serializer<T> serializer) {
		this.tables = tables;
		this.name = name;
		this.serializer = serializer;
	}

	@Override
	public DatabaseBuilder add(List<T> objects) throws Exception {
		Parser<T> parser = new Parser<>(serializer);
		Storage storage = storageFactory.create(parser);
		Table<T> table = tableFactory.create(serializer, objects, storage, parser);
		tables.put(name, table);
		return dbBuilderFactory.create(tables);
	}
}

interface TableBuilderFactory {
	<T> TableBuilder<T> create(Map<String, Table<?>> tables, String name, Serializer<T> serializer);
}

class TableBuilderFactoryImpl implements TableBuilderFactory {
	@Override
	public <T> TableBuilder<T> create(Map<String, Table<?>> tables, String name, Serializer<T> serializer) {
		TableBuilder<T> tableBuilder = new TableBuilderDefault<T>(tables, name, serializer);
		DatabaseModule.getInjector().injectMembers(tableBuilder);
		return tableBuilder;
	}
}