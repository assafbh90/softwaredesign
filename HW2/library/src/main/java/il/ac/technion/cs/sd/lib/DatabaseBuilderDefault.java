package il.ac.technion.cs.sd.lib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

public class DatabaseBuilderDefault implements DatabaseBuilder {
	@Inject
	private DatabaseFactory databaseFactory;
	@Inject
	private TableBuilderFactory tableBuilderFactory;

	final private Map<String, Table<?>> tables;

	@Inject
	public DatabaseBuilderDefault() {
		this.tables = new HashMap<String, Table<?>>();
	}
	
	DatabaseBuilderDefault(Map<String, Table<?>> tables) {
		this.tables = tables;
	}

	@Override
	public <T> TableBuilder<T> addTable(String name, Serializer<T> serializer) {
		return tableBuilderFactory.create(tables, name, serializer);
	}

	@Override
	public <T> DatabaseBuilder addTable(String name, Serializer<T> serializer, List<T> objects) throws Exception {
		return tableBuilderFactory.create(tables, name, serializer).add(objects);
	}

	@Override
	public Database Build() throws Exception {
		return databaseFactory.create(tables);
	}
}

interface DatabaseBuilderFactory {
	DatabaseBuilder create(Map<String, Table<?>> tables);
}

class DatabaseBuilderFactoryImpl implements DatabaseBuilderFactory {

	@Override
	public DatabaseBuilder create(Map<String, Table<?>> tables) {
		DatabaseBuilder DBuilder = new DatabaseBuilderDefault(tables);
		DatabaseModule.getInjector().injectMembers(DBuilder);
		return DBuilder;
	}
	
}
