package il.ac.technion.cs.sd.lib;

import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

public class DatabaseDefault implements Database {
	private Map<String, Table<?>> tables;

	@Inject
	DatabaseDefault(@Assisted Map<String, Table<?>> tables) throws Exception {
		this.tables = tables;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Table<T> from(String table) throws TableNotFound {
		Optional<Table<T>> t = Optional.ofNullable((Table<T>)tables.get(table));
		if (!t.isPresent()) throw new TableNotFound();
		return t.get();
	}
}

interface DatabaseFactory {
	DatabaseDefault create(Map<String, Table<?>> tables);
}