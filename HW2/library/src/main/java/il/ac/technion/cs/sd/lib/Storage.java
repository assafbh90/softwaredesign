package il.ac.technion.cs.sd.lib;

import il.ac.technion.cs.sd.book.ext.LineStorage;

public interface Storage extends LineStorage {
	public String readValue(int line) throws InterruptedException;
}
