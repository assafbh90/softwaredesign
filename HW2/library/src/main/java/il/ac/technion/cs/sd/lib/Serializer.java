package il.ac.technion.cs.sd.lib;

import java.util.List;

public interface Serializer<T> {
	T objectFactory(String key, List<String> value);

	String getKey(T object);

	List<String> getValue(T object);
}
