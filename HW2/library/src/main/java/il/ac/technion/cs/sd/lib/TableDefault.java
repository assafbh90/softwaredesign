package il.ac.technion.cs.sd.lib;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TableDefault<T> implements Table<T> {

	private Serializer<T> ser;
	private Storage storage;
	private Parser<T> parser;
	private int size;

	TableDefault(Serializer<T> ser, List<T> objects, Storage storage, Parser<T> parser) throws Exception {
		this.ser = ser;
		this.storage = storage;
		this.parser = parser;
		Collections.reverse(objects);
		objects.stream().filter(distinctByKey(ser::getKey)).sorted(Comparator.comparing(ser::getKey))
				.forEachOrdered(this::addLast);
		this.size = storage.numberOfLines();
	}

	@Override
	public Optional<T> get(String key) throws Exception {
		int low = 0;
		int high = size - 1;
		while (low <= high) {
			int middle = low + (high - low) / 2;
			String result = storage.read(middle);
			if (result == "") {
				return Optional.empty();
			}
			String keyFromDB = unescape(parser.getKey(result));
			if (key.compareTo(keyFromDB) < 0) {
				high = middle - 1;
			} else if (key.compareTo(keyFromDB) > 0) {
				low = middle + 1;
			} else {
				Optional<String> optional = Optional.ofNullable(storage.readValue(middle));
				List<String> valueFromDB = parser.getValue(optional.get()).stream().map(s -> unescape(s)).collect(Collectors.toList());
				return Optional.ofNullable(ser.objectFactory(keyFromDB, valueFromDB));
			}
		}
		return Optional.empty();
	}

	@Override
	public int size() throws Exception {
		return size;
	}

	private void addLast(T object) {
		String key = escape(ser.getKey(object));
		List<String> values = ser.getValue(object).stream().map(s -> escape(s)).collect(Collectors.toList());
		storage.appendLine(parser.toCsv(key, values));
	}

	private <R> Predicate<R> distinctByKey(Function<? super R, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> !Optional.ofNullable(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE)).isPresent();
	}
	private final char EscapeChar = 14;
	private String escape(String s) {
		return s.replace(',', EscapeChar);
	}
	private String unescape(String s) {
		return s.replace(EscapeChar, ',');
	}
}

interface TableFactory {
	<T> Table<T> create(Serializer<T> ser, List<T> objects, Storage storage, Parser<T> parser) throws Exception;
}

class TableFactoryImpl implements TableFactory {
	@Override
	public <T> Table<T> create(Serializer<T> ser, List<T> objects, Storage storage, Parser<T> parser) throws Exception {
		Table<T> table = new TableDefault<T>(ser, objects, storage, parser);
		DatabaseModule.getInjector().injectMembers(table);
		return table;
	}
}