package il.ac.technion.cs.sd.lib;

import java.util.ArrayList;
import java.util.List;

import il.ac.technion.cs.sd.book.ext.LineStorage;

public class LineStorageFake implements LineStorage {
	List<String> storage = new ArrayList<String>();
	@Override
	public void appendLine(String s) {
		storage.add(s);
	}

	@Override
	public String read(int lineNumber) throws InterruptedException {
		String s = storage.get(lineNumber);
		Thread.sleep(s.length());
		return s;
	}

	@Override
	public int numberOfLines() throws InterruptedException {
		Thread.sleep(100);
		return storage.size();
	}

}
