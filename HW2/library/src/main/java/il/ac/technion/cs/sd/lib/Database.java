package il.ac.technion.cs.sd.lib;

public interface Database {
	@SuppressWarnings("serial")
	class TableNotFound extends Exception{};
	<T> Table<T> from(String table) throws TableNotFound;
}
