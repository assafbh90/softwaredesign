package il.ac.technion.cs.sd.lib;

import java.util.List;

public interface DatabaseBuilder {
	<T> TableBuilder<T> addTable(String name, Serializer<T> serializer);

	<T> DatabaseBuilder addTable(String name, Serializer<T> serializer, List<T> objects) throws Exception;

	Database Build() throws Exception;
}
