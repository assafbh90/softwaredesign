package il.ac.technion.cs.sd.lib;

import com.google.inject.Inject;
import il.ac.technion.cs.sd.book.ext.LineStorage;

class StorageDefault<T> implements Storage {
	@Inject
	private LineStorage keyStorage;
	@Inject
	private LineStorage valueStorage;
	final private Parser<T> parser;

	StorageDefault(Parser<T> parser) {
		this.parser = parser;
	}

	@Override
	public void appendLine(String line) {
		keyStorage.appendLine(parser.getKey(line));
		valueStorage.appendLine(parser.toCsv(parser.getValue(line)));
	}

	@Override
	public int numberOfLines() throws InterruptedException {
		return keyStorage.numberOfLines();
	}

	@Override
	public String read(int line) throws InterruptedException {
		return keyStorage.read(line);
	}

	@Override
	public String readValue(int line) throws InterruptedException {
		return valueStorage.read(line);
	}
}

interface StorageFactory {
	<T> Storage create(Parser<T> parser);
}

class StorageFactoryImpl implements StorageFactory {

	@Override
	public <T> Storage create(Parser<T> parser) {
		Storage storage = new StorageDefault<T>(parser);
		DatabaseModule.getInjector().injectMembers(storage);
		return storage;
	}
	
}