package il.ac.technion.cs.sd.lib;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import il.ac.technion.cs.sd.lib.Parser;
import il.ac.technion.cs.sd.lib.Serializer;

public class ParserParseTests {
	
	private static final int LAST_REGULAR_STUDENT = 10;
	private static final int FIRST_REGULAR_STUDENT = 0;
	private static final int LAST_3FIELD_STUDENT = 14;
	private static final int FIRST_3FIELD_STUDENT = 12;
	private static final int BAD_STUDENT_GRADE = 11;
	private static final int BAD_STUDENT_ID = 10;
	private final String[] students = {"123456789,100",
										"0123,90",
										"000,93",
										"100,100",
										"200,90",
										"123123,23",
										"1231231,23",
										"100,100",
										"200,90",
										"200,30",
										"abc,16",
										"16,abc",
										"200,30,150",
										"100,16,100",
										};
	private static final String FIELD_SEPARATOR = ",";
	private final static int ID = 0;
	private final static int GRADE = 1;
	
	@Mock
	Serializer<Student> s = new StudentSerializer();
	
	@Test 
	public void nothing() {
		List<Student> list;
		
		list = new Parser<Student>(s).Parse("");
		
		assertEquals(0, list.size());
	}
	
	@Test 
	public void oneStudent() {
		List<Student> list;
		
		list = new Parser<Student>(s).Parse(students[0]);
		
		assertEquals(1, list.size());
		assertEquals(students[0].split(FIELD_SEPARATOR)[ID], list.get(0).getId());
		assertEquals(Integer.parseInt(students[0].split(FIELD_SEPARATOR)[GRADE]), list.get(0).getGrade().intValue());
	}
	
	@Test 
	public void manyStudent() {
		List<Student> list;
		String students = "";
		
		for (int i = FIRST_REGULAR_STUDENT; i < LAST_REGULAR_STUDENT ; ++i) {
			students += this.students[i] + "\n";
		}
		
		list = new Parser<Student>(s).Parse(students);
		
		assertEquals(LAST_REGULAR_STUDENT - FIRST_REGULAR_STUDENT, list.size());
		for (int i = FIRST_REGULAR_STUDENT; i < LAST_REGULAR_STUDENT ; ++i) {
			assertEquals(this.students[i].split(FIELD_SEPARATOR)[ID], list.get(i).getId());
			assertEquals(Integer.parseInt(this.students[i].split(FIELD_SEPARATOR)[GRADE]), list.get(i).getGrade().intValue());
		}
	}
	
	@Test(expected = NumberFormatException.class)
	public void badStudentId() {
		new Parser<Student>(s).Parse(students[BAD_STUDENT_ID]);
	}
	
	@Test(expected = NumberFormatException.class)
	public void badStudentGrade() {
		new Parser<Student>(s).Parse(students[BAD_STUDENT_GRADE]);
	}
	
	@Test 
	public void tooManyFields() {
		List<Student> list;
		String students = "";
		
		for (int i = FIRST_3FIELD_STUDENT; i < LAST_3FIELD_STUDENT ; ++i) {
			students += this.students[i] + System.lineSeparator();
		}
		
		list = new Parser<Student>(s).Parse(students);
		
		assertEquals(LAST_3FIELD_STUDENT - FIRST_3FIELD_STUDENT, list.size());
		for (int i = FIRST_3FIELD_STUDENT; i < LAST_3FIELD_STUDENT; ++i) {
			assertEquals(this.students[i].split(FIELD_SEPARATOR)[ID], list.get(i-FIRST_3FIELD_STUDENT).getId());
			assertEquals(Integer.parseInt(this.students[i].split(FIELD_SEPARATOR)[GRADE]), list.get(i-FIRST_3FIELD_STUDENT).getGrade().intValue());
		}
	}
}
