package il.ac.technion.cs.sd.lib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import org.junit.Test;

import com.google.inject.Injector;

//import il.ac.technion.cs.sd.book.app.Critic;

public class DatabaseBuilderTest {
	Critic c1 = new Critic("Bob");
	
	class Critic {
		public Critic(String name) { this.name = name; }
		public String name;
		public List<String> v = new ArrayList<>();
	}
	class CriticSerializer implements Serializer<Critic> {
		@Override
		public Critic objectFactory(String key, List<String> value) {
			Critic critic = new Critic(key);
			value.stream().map(x -> x.split(",")).forEach(x -> critic.v.add((x[0])));
			return critic;
		}

		@Override
		public String getKey(Critic critic) {
			return critic.name;
		}

		@Override
		public List<String> getValue(Critic critic) {
			return critic.v
					.stream()
					.map(x -> x + ",")
					.collect(Collectors.toList());
		}
	}
	@Test
	public void build2tables_ckeck_equality() throws Exception {
		DatabaseBuilder dbb = DatabaseModule.getInjector().getInstance(DatabaseBuilder.class);
		Database db = dbb.addTable("A", new CriticSerializer())
		.add(Arrays.asList(c1))
		.addTable("B", new CriticSerializer())
		.add(Arrays.asList())
		.Build();
		Critic c = (Critic) db.from("A").get("Bob").get();
		assertEquals(c.name, "Bob");
	}
	@Test(expected = Exception.class)
	public void keyIsNotPresent() throws Exception {
		DatabaseBuilder dbb = DatabaseModule.getInjector().getInstance(DatabaseBuilder.class);
		Database db = dbb.addTable("A", new CriticSerializer())
		.add(Arrays.asList(c1)).addTable("B", new CriticSerializer())
		.add(Arrays.asList())
		.Build();
		db.from("A").get("X").get();
	}
}
