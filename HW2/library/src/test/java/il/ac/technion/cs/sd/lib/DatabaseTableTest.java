package il.ac.technion.cs.sd.lib;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import il.ac.technion.cs.sd.lib.TableDefault;
import il.ac.technion.cs.sd.lib.Parser;
import il.ac.technion.cs.sd.lib.Serializer;
import il.ac.technion.cs.sd.lib.StorageDefault;

@SuppressWarnings("deprecation")
public class DatabaseTableTest {
	private TableDefault<Student> db;
	private Serializer<Student> ser;
	private Storage storage;
	private Parser<Student> parser;
	
	private final List<Student> studentsFixed =
			Arrays.asList(
				new Student("123456789,95"),
				new Student("123,80"),
				new Student("789,90"),
				new Student("111,70"),
				new Student("000,100")
			);	
	private final List<Student> studentsDuplicate =
			Arrays.asList(
				new Student("123456789,95"),
				new Student("123456789,85"),
				new Student("123456789,65"),
				new Student("123,80"),
				new Student("123,70"),
				new Student("123,60"),
				new Student("789,90"),
				new Student("789,80"),
				new Student("789,70"),
				new Student("111,70"),
				new Student("111,60"),
				new Student("111,50")
			);

	@Before
	public void INIT(){
		parser = new Parser<Student>(new StudentSerializer());
		ser = new StudentSerializer();
	}

	public void initTest(List<Student> students_for_mock, List<Student> student_for_db) throws Exception {
		storage = Mockito.mock(StorageDefault.class);

		Mockito.when(storage.numberOfLines()).thenReturn(students_for_mock.size());		
		for (int i = 0; i < storage.numberOfLines(); ++i) {
			Mockito.when(storage.read(i)).thenReturn(ser.getKey(students_for_mock.get(i)));
			Mockito.when(storage.readValue(i)).thenReturn(parser.toCsv(ser.getValue(students_for_mock.get(i))));
		}
		
		db = new TableDefault<Student>(ser, student_for_db, storage,new Parser<Student>(ser));
	}
	
	@Test
	public void checkGetNotDuplicate() throws Exception {
		List<Student> students = studentsFixed.stream().sorted(Comparator.comparing(ser::getKey)).collect(Collectors.toList());
		initTest(students, studentsFixed);
		
		getGoodFlow(studentsFixed);
		getBadFlow();
	}
	
	@Test
	public void checkAppendDuplicates() throws Exception {
		List<Student> students = studentsDuplicate.stream().sorted(Comparator.comparing(ser::getKey)).collect(Collectors.toList());
		initTest(students, studentsDuplicate);
		
		Mockito.verify(storage).appendLine("111,50");
		Mockito.verify(storage).appendLine("123,60");
		Mockito.verify(storage).appendLine("123456789,65");
		Mockito.verify(storage).appendLine("789,70");
		Mockito.verify(storage, VerificationModeFactory.times(4)).appendLine(Matchers.any(String.class));
	}
	@Test
	public void checkAppendFixed() throws Exception {
		List<Student> students = studentsFixed.stream().sorted(Comparator.comparing(ser::getKey)).collect(Collectors.toList());
		initTest(students, studentsFixed);
		
		Mockito.verify(storage).appendLine("000,100");
		Mockito.verify(storage).appendLine("111,70");
		Mockito.verify(storage).appendLine("123,80");
		Mockito.verify(storage).appendLine("123456789,95");
		Mockito.verify(storage).appendLine("789,90");
		Mockito.verify(storage, VerificationModeFactory.times(5)).appendLine(Matchers.any(String.class));
	}
	
	@Test
	public void checkNumOfLines() throws Exception {
		Serializer<Student> ser = new StudentSerializer();;
		Storage storage = Mockito.mock(StorageDefault.class);
		parser = new Parser<Student>(ser);
		Mockito.when(storage.numberOfLines()).thenReturn(studentsFixed.size());
		TableDefault<Student> db = new TableDefault<Student>(ser, studentsFixed, storage,new Parser<Student>(ser));
		
		assertEquals(studentsFixed.size(), db.size());
		Mockito.verify(storage, VerificationModeFactory.times(1)).numberOfLines();
	}
	
	private void getGoodFlow(List<Student> students) throws Exception {
		assertEquals(db.size(), students.size());
		for (int i = 0; i < storage.numberOfLines(); ++i) {
			Student student_from_list = students.get(i);
			Student student_from_db = db.get(ser.getKey(student_from_list)).get();

			assertEquals(ser.getValue(student_from_list).get(0), ser.getValue(student_from_db).get(0));
		}
	}

	private void getBadFlow() throws Exception {
		for (int i = 1; i < 50; ++i) {
			Optional<Student> student_from_db = db.get(ser.getKey(new Student("" + i, i)));
			assertEquals(Optional.empty(), student_from_db);
		}
	}
	
	@Test
	public void checkGetDuplicate() throws Exception {
		List<Student> students = studentsDuplicate.stream()
								.sorted(Comparator.comparing(ser::getKey))
								.collect(Collectors.toList());
		List<Student> seen = new LinkedList<Student>();
		for (int i = students.size() - 1; i >= 0; i--){
			if(!seen.contains(students.get(i))) {
				seen.add(students.get(i));
				students.remove(i);
			}
		}
		initTest(students, studentsDuplicate);
		
		getGoodFlow(students);
	}
}
