package il.ac.technion.cs.sd.lib;

import java.util.Arrays;
import java.util.List;

import il.ac.technion.cs.sd.lib.Serializer;

public class StudentSerializer implements Serializer<Student> {

	@Override
	public Student objectFactory(String key, List<String> value) {
		return new Student(key, Integer.parseInt(value.get(0)));
	}

	@Override
	public String getKey(Student object) {
		return object.getId().toString();
	}

	@Override
	public List<String> getValue(Student object) {
		return Arrays.asList(object.getGrade().toString());
	}
	
}
