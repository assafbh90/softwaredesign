package il.ac.technion.cs.sd.book.app;

import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.lib.Serializer;

public class CriticSerializer implements Serializer<Critic> {
	@Override
	public Critic objectFactory(String key, List<String> value) {
		Critic critic = new Critic(key);
		value.stream().map(x -> x.split(",")).forEach(x -> critic.addReview(x[0], x[1]));
		return critic;
	}

	@Override
	public String getKey(Critic critic) {
		return critic.getId();
	}

	@Override
	public List<String> getValue(Critic critic) {
		return critic.getReviews()
				.stream()
				.map(x -> x.getId() + "," + x.getScore())
				.collect(Collectors.toList());
	}
}
