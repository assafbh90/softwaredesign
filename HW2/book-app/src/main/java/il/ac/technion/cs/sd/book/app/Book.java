package il.ac.technion.cs.sd.book.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Book {
	private String id;
	private Map<String, Reviewer> reviewers;

	public Book(String id) {
		this.id = id;
		this.reviewers = new HashMap<>();
	}

	public String getId() {
		return id;
	}

	public List<Reviewer> getReviewers() {
		return reviewers.values().stream().collect(Collectors.toList());
	}

	public Book addReviewer(String id, String score) {
		reviewers.put(id, new Reviewer(id, score));
		return this;
	}
	public Optional<Reviewer> findReviewer(String id) {
		return Optional.ofNullable(reviewers.get(id));
	}
}
