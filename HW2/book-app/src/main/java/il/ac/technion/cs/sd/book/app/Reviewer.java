package il.ac.technion.cs.sd.book.app;

public class Reviewer {
	private String id;
	private Double score;

	public Reviewer(String id, Double score) {
		this.id = id;
		this.score = score;
	}

	public String getId() {
		return id;
	}

	public Double getScore() {
		return score;
	}

	public Reviewer(String id, String score) {
		this.id = id;
		this.score = Double.valueOf(score);
	}
	
}
