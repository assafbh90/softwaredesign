package il.ac.technion.cs.sd.book.app;

import javax.xml.parsers.DocumentBuilderFactory;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.inject.Inject;

import il.ac.technion.cs.sd.lib.DatabaseBuilder;
import il.ac.technion.cs.sd.lib.DatabaseModule;
import il.ac.technion.cs.sd.lib.LibraryManager;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class BookScoreInitializerImpl implements BookScoreInitializer {
	private LibraryManager lib;
	
	@Inject
	private BookScoreInitializerImpl(LibraryManager lib) {
		this.lib = lib;
	}
	@Override
	public void setup(String xmlData) throws Exception {
		Map<String, Book> books = new HashMap<>();
		Map<String, Critic> critics = new HashMap<>();

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(xmlData)));

			doc.getDocumentElement().normalize();

			NodeList Reviewers = doc.getElementsByTagName("Reviewer");

			for (int i = 0; i < Reviewers.getLength(); i++) {
				Node Reviewer = Reviewers.item(i);

				if (Reviewer.getNodeType() == Node.ELEMENT_NODE) {
					Element reviewerElement = (Element) Reviewer;
					NodeList Reviews = reviewerElement.getElementsByTagName("Review");

					for (int j = 0; j < Reviews.getLength(); j++) {
						Node Review = Reviews.item(j);

						if (Review.getNodeType() == Node.ELEMENT_NODE) {
							Element reviewElement = (Element) Review;

							String criticId = reviewerElement.getAttribute("Id");
							String bookId = reviewElement.getElementsByTagName("Id").item(0).getTextContent();
							String score = reviewElement.getElementsByTagName("Score").item(0).getTextContent();

							// Critic handle
							if (critics.containsKey(criticId)) {
								critics.put(criticId, critics.get(criticId).addReview(bookId, score));
							} else {
								critics.put(criticId, new Critic(criticId).addReview(bookId, score));
							}
							
							// Book handle
							if (books.containsKey(bookId)) {
								books.put(bookId, books.get(bookId).addReviewer(criticId, score));
							} else {
								books.put(bookId, new Book(bookId).addReviewer(criticId, score));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Book> booksList = books.values().stream().collect(Collectors.toList());
		List<Critic> criticsList = critics.values().stream().collect(Collectors.toList());
		
		DatabaseBuilder dbBuilder = DatabaseModule.getInjector().getInstance(DatabaseBuilder.class);
		
		lib.setDatabase(lib.Builder()
				.addTable("books", new BookSerializer()).add(booksList)
				.addTable("critics", new CriticSerializer()).add(criticsList)
				.Build());
	}
}
