package il.ac.technion.cs.sd.book.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Critic {
	private String id;
	private Map<String, Review> reviews;

	public Critic(String id) {
		this.id = id;
		this.reviews = new HashMap<>();
	}

	public String getId() {
		return id;
	}

	public List<Review> getReviews() {
		return reviews.values().stream().collect(Collectors.toList());
	}

	public Critic addReview(String id, String score) {
		reviews.put(id, new Review(id, score));
		return this;
	}
	public Optional<Review> findReview(String id) {
		return Optional.ofNullable(reviews.get(id));
	}	
}
