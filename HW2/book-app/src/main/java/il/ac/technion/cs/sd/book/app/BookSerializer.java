package il.ac.technion.cs.sd.book.app;

import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.lib.Serializer;

public class BookSerializer implements Serializer<Book> {
	@Override
	public Book objectFactory(String key, List<String> value) {
		Book book = new Book(key);
		value.stream().map(x -> x.split(",")).forEach(x -> book.addReviewer(x[0], x[1]));
		return book;
	}

	@Override
	public String getKey(Book book) {
		return book.getId();
	}

	@Override
	public List<String> getValue(Book book) {
		return book.getReviewers()
				.stream()
				.map(x -> x.getId() + "," + x.getScore())
				.collect(Collectors.toList());
	}
}
