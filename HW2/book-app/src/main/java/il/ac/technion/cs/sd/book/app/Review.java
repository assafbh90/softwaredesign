package il.ac.technion.cs.sd.book.app;

public class Review {
	private String id;
	private Double score;

	public String getId() {
		return id;
	}

	public Double getScore() {
		return score;
	}

	public Review(String id, Double score) {
		this.id = id;
		this.score = score;
	}

	public Review(String id, String score) {
		this.id = id;
		this.score = Double.valueOf(score);
	}
}
