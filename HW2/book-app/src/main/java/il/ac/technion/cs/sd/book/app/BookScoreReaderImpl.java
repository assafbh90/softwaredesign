package il.ac.technion.cs.sd.book.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import com.google.inject.Inject;

import il.ac.technion.cs.sd.lib.Database;
import il.ac.technion.cs.sd.lib.Database.TableNotFound;
import il.ac.technion.cs.sd.lib.LibraryManager;

public class BookScoreReaderImpl implements BookScoreReader {
	private static final String BOOKS = "books";
	private static final String CRITICS = "critics";
	private Database db;

	@Inject
	private BookScoreReaderImpl(LibraryManager lib) {
		Optional<Database> optional = lib.getDatabase();
		if (optional.isPresent()) {
			db = optional.get();
		} else {
			throw new Error("Database not present yet");
		}
	}

	@Override
	public boolean gaveReview(String reviewerId, String bookId) {
		try {
			Optional<Object> optional = db.from(CRITICS).get(reviewerId);
			if (optional.isPresent()) {
				Critic critic = (Critic) optional.get();
				return critic.findReview(bookId).isPresent();
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public OptionalDouble getScore(String reviewerId, String bookId) {
		try {
			Optional<Object> optional = db.from(CRITICS).get(reviewerId);
			if (optional.isPresent()) {
				Critic critic = (Critic) optional.get();
				Optional<Review> review = critic.findReview(bookId);
				if (review.isPresent()) {
					return OptionalDouble.of(review.get().getScore());
				}
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OptionalDouble.empty();
	}

	@Override
	public List<String> getReviewedBooks(String reviewerId) {
		try {
			Optional<Object> optional = db.from(CRITICS).get(reviewerId);
			if (optional.isPresent()) {
				List<Review> reviews = ((Critic) optional.get()).getReviews();
				return reviews.stream().map(x -> x.getId()).sorted().collect(Collectors.toList());
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	@Override
	public Map<String, Integer> getAllReviewsByReviewer(String reviewerId) {
		try {
			Optional<Object> optional = db.from(CRITICS).get(reviewerId);

			if (optional.isPresent()) {
				List<Review> reviews = ((Critic) optional.get()).getReviews();
				return reviews.stream().collect(Collectors.toMap(Review::getId, x -> x.getScore().intValue()));
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}

	@Override
	public OptionalDouble getScoreAverageForReviewer(String reviewerId) {
		try {
			Optional<Object> optional = db.from(CRITICS).get(reviewerId);

			if (optional.isPresent()) {
				List<Review> reviews = ((Critic) optional.get()).getReviews();
				if (reviews.isEmpty())
					return OptionalDouble.empty();
				return OptionalDouble.of(reviews.stream().map(x -> x.getScore()).reduce(new Double(0), (a, b) -> a + b)
						/ reviews.size());
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OptionalDouble.empty();
	}

	@Override
	public List<String> getReviewers(String bookId) {
		try {
			Optional<Object> optional = db.from(BOOKS).get(bookId);

			if (optional.isPresent()) {
				List<Reviewer> reviewers = ((Book) optional.get()).getReviewers();
				return reviewers.stream().map(x -> x.getId()).sorted().collect(Collectors.toList());
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	@Override
	public Map<String, Integer> getReviewsForBook(String bookId) {
		try {
			Optional<Object> optional = db.from(BOOKS).get(bookId);
			if (optional.isPresent()) {
				List<Reviewer> reviewers = ((Book) optional.get()).getReviewers();
				return reviewers.stream().collect(Collectors.toMap(Reviewer::getId, x -> x.getScore().intValue()));
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}

	@Override
	public OptionalDouble getAverageReviewScoreForBook(String bookId) {
		Optional<Object> optional;
		try {
			optional = db.from(BOOKS).get(bookId);
			if (optional.isPresent()) {
				List<Reviewer> reviewers = ((Book) optional.get()).getReviewers();
				if (reviewers.isEmpty())
					return OptionalDouble.empty();
				return OptionalDouble
						.of(reviewers.stream().map(x -> x.getScore()).reduce(new Double(0), (a, b) -> a + b)
								/ reviewers.size());
			}
		} catch (TableNotFound e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OptionalDouble.empty();

	}

}
