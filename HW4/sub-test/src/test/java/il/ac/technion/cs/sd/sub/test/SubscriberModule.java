package il.ac.technion.cs.sd.sub.test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import guice.libModule;
import il.ac.technion.cs.sd.sub.app.SubscriberInitializer;
import il.ac.technion.cs.sd.sub.app.SubscriberInitializerImpl;
import il.ac.technion.cs.sd.sub.app.SubscriberReader;
import il.ac.technion.cs.sd.sub.app.SubscriberReaderImpl;
import storage.Storages.StringStorages.LibManager;

// This module is in the testing project, so that it could easily bind all dependencies from all levels.
public class SubscriberModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(SubscriberInitializer.class).to(SubscriberInitializerImpl.class);
		bind(SubscriberReader.class).to(SubscriberReaderImpl.class);
		bind(LibManager.class).toInstance(Guice.createInjector(new libModule()).getInstance(LibManager.class));
	}
}
