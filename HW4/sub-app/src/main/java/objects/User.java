package objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {

	String id;
	List<String> currentJournals;
	Map<String,List<Boolean>> history; 
	Integer amountOfMoney;
	public User(String id){
		this.id = id;
		this.currentJournals = new ArrayList<>();
		this.history = new HashMap<>();
		this.amountOfMoney = 0;
	}

	public void register(Magazine jornal) {
		String jornalId = jornal.getId();
		Integer price = jornal.getPrice();
		if (!currentJournals.contains(jornalId)) {
			currentJournals.add(jornalId);
			amountOfMoney += price;
		}
		List<Boolean> currentHistory;
		if (history.containsKey(jornalId)) {
			currentHistory = this.history.get(jornalId);
		} else {
			currentHistory = new ArrayList<>();
		}
		currentHistory.add(true);
		this.history.put(jornalId, currentHistory);
	}

	public void cancel(Magazine jornal) {
		String jornalId = jornal.getId();
		Integer price = jornal.getPrice();
		if (currentJournals.contains(jornalId)) {
			currentJournals.remove(jornalId);
			amountOfMoney -= price;
		}
		if (history.containsKey(jornalId)) {
			List<Boolean> history = this.history.get(jornalId);
			if (history.get(history.size() - 1)) {
				history.add(false);
				this.history.put(jornalId, history);
			}
		} else {
			List<Boolean> currentHistory = new ArrayList<>();
			currentHistory.add(false);
			this.history.put(jornalId, currentHistory);
		}
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getCurrentJournals() {
		return currentJournals;
	}
	public void setCurrentJournals(List<String> currentJournals) {
		this.currentJournals = currentJournals;
	}
	public Map<String, List<Boolean>> getHistory() {
		return history;
	}
	public void setHistory(Map<String, List<Boolean>> history) {
		this.history = history;
	}
	public Integer getAmountOfMoney() {
		return amountOfMoney;
	}
	public void setAmountOfMoney(Integer amountOfMoney) {
		this.amountOfMoney = amountOfMoney;
	}
}
