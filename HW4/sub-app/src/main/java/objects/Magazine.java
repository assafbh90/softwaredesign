package objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazine {

	String id;
	Integer price;
	List<String> currentUsers;
	Map<String, List<Boolean>> history;
	Integer amountOfMoney;

	public Magazine(String id, Integer price) {
		this.id = id;
		this.price = price;
		this.currentUsers = new ArrayList<>();
		this.history = new HashMap<>();
		this.amountOfMoney = 0;
	}

	public Magazine(String id, String price) {
		this.id = id;
		this.price = Integer.valueOf(price);
	}

	public void register(User user) {
		String userId = user.getId();
		if (!currentUsers.contains(userId)) {
			currentUsers.add(userId);
			amountOfMoney += price;
		}
		List<Boolean> currentHistory;
		if (history.containsKey(userId)) {
			currentHistory = this.history.get(userId);
		} else {
			currentHistory = new ArrayList<>();
		}
		currentHistory.add(true);
		this.history.put(userId, currentHistory);
	}

	public void cancel(User user) {
		String userId = user.getId();
		if (currentUsers.contains(userId)) {
			currentUsers.remove(userId);
			amountOfMoney -= price;
		}
		if (history.containsKey(userId)) {
			List<Boolean> history = this.history.get(userId);
			if (history.get(history.size() - 1)) {
				history.add(false);
				this.history.put(userId, history);
			}
		} else {
			List<Boolean> currentHistory = new ArrayList<>();
			currentHistory.add(false);
			this.history.put(userId, currentHistory);
		}
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public List<String> getCurrentUsers() {
		return currentUsers;
	}

	public void setCurrentUsers(List<String> currentUsers) {
		this.currentUsers = currentUsers;
	}

	public Map<String, List<Boolean>> getHistory() {
		return history;
	}

	public void setHistory(Map<String, List<Boolean>> history) {
		this.history = history;
	}

	public Integer getAmountOfMoney() {
		return amountOfMoney;
	}

	public void setAmountOfMoney(Integer amountOfMoney) {
		this.amountOfMoney = amountOfMoney;
	}

}
