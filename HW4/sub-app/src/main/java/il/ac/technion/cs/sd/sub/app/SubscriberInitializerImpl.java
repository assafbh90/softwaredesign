package il.ac.technion.cs.sd.sub.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.inject.Inject;

import objects.Magazine;
import objects.User;
import storage.Storages.FutureStorage;
import storage.Storages.StringStorages.LibManager;

public class SubscriberInitializerImpl implements SubscriberInitializer {
	private static final String JORNAL2USER_AND_HISTORY = "jornal2userAndHistory";
	private static final String JORNAL2MONEY = "jornal2money";
	private static final String JORNAL2USERS = "jornal2users";
	private static final String USER2MONEY = "user2money";
	private static final String USER2JORNAL_AND_HISTORY = "user2jornalAndHistory";
	private static final String USER2CURRENT_JORNALS = "user2currentJornals";
	LibManager lib;
	@Inject
	public SubscriberInitializerImpl(LibManager lib) {
		this.lib = lib;
	}
	@Override
	public CompletableFuture<Void> setupCsv(String csvData) {
		Map<String, Magazine> magazines = new HashMap<>();
		Map<String, User> users = new HashMap<>();
		
		csvData.replace("\r", "");
		List<String[]> data = Arrays.asList(csvData.split("\n")).stream().map(v->v.trim().split(",")).collect(Collectors.toList());

		for (String[] s : data) {
			if (s[0].equals("journal")) {
				String jornalId = s[1];
				String price = s[2];
				magazines.put(jornalId, new Magazine(jornalId, Integer.valueOf(price)));
			}
		}
		for (String[] s : data) {
			if (!s[0].equals("journal")) {
				String userId = s[1];
				users.put(userId, new User(userId));
			}
		}
		
		for (String[] s : data) {
			
			if (s[0].equals("journal")) continue;
			String userId = s[1];
			String jornalId = s[2];
			
			if(!magazines.containsKey(jornalId)) continue;
			Magazine magazine = magazines.get(jornalId);
			User user;
			
			if (users.containsKey(userId)) {
				user = users.get(userId);
			} else {
				user = new User(userId);
			}
			
			switch (s[0]) {
			case "subscriber":
				user.register(magazine);
				magazine.register(user);
				
				break;
			case "cancel":
				user.cancel(magazine);
				magazine.cancel(user);
				
				break;
			default:
				break;
			}
			
			magazines.put(magazine.getId(), magazine);
			users.put(user.getId(), user);
		}
		
		return CreateDatabase(magazines, users);
	}

	@Override
	public CompletableFuture<Void> setupJson(String jsonData) {
			Map<String, Magazine> magazines = new HashMap<>();
			Map<String, User> users = new HashMap<>();
			
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = null;
			try {
				jsonArray = (JSONArray) parser.parse(jsonData);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				if (jsonObject.get("type").equals("journal")) {
					String jornalId = jsonObject.get("journal-id").toString();
					String price = jsonObject.get("price").toString();
					magazines.put(jornalId, new Magazine(jornalId, Integer.valueOf(price)));
				}
			}
			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				if (!jsonObject.get("type").equals("journal")) {
					String userId = jsonObject.get("user-id").toString();
					users.put(userId, new User(userId));
				}
			}
			

			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				
				if (jsonObject.get("type").equals("journal")) continue;
				String userId = jsonObject.get("user-id").toString();
				String jornalId = jsonObject.get("journal-id").toString();
				
				if(!magazines.containsKey(jornalId)) continue;
				Magazine magazine = magazines.get(jornalId);
				User user;
				
				if (users.containsKey(userId)) {
					user = users.get(userId);
				} else {
					user = new User(userId);
				}
				
				switch (jsonObject.get("type").toString()) {
				case "subscription":
					user.register(magazine);
					magazine.register(user);
					
					break;
				case "cancel":
					user.cancel(magazine);
					magazine.cancel(user);
					break;
				default:
					break;
				}
				
				magazines.put(magazine.getId(), magazine);
				users.put(user.getId(), user);
			}
	
		return CreateDatabase(magazines, users);
	}
	private CompletableFuture<Void> CreateDatabase(Map<String, Magazine> magazines, Map<String, User> users) {
		FutureStorage<String> user2currentJornals = lib.add(USER2CURRENT_JORNALS);
		FutureStorage<String> user2jornalAndHistory = lib.add(USER2JORNAL_AND_HISTORY);
		FutureStorage<String> user2money = lib.add(USER2MONEY);
		
		FutureStorage<String> jornal2users = lib.add(JORNAL2USERS);
		FutureStorage<String> jornal2money= lib.add(JORNAL2MONEY);
		FutureStorage<String> jornal2userAndHistory = lib.add(JORNAL2USER_AND_HISTORY);
		
		users.entrySet().stream().forEach(e-> user2currentJornals.loadData(e.getKey(), e.getValue().getCurrentJournals()));
		users.entrySet().stream().forEach(e-> user2jornalAndHistory.loadData(e.getKey(), History2csvTuple(e.getValue().getHistory())));
		users.entrySet().stream().forEach(e-> user2money.loadData(e.getKey(), Arrays.asList("" + e.getValue().getAmountOfMoney())));
		
		magazines.entrySet().stream().forEach(e->jornal2users.loadData(e.getKey(), e.getValue().getCurrentUsers()));
		magazines.entrySet().stream().forEach(e->jornal2money.loadData(e.getKey(), Arrays.asList("" + e.getValue().getAmountOfMoney())));
		magazines.entrySet().stream().forEach(e->jornal2userAndHistory.loadData(e.getKey(), History2csvTuple(e.getValue().getHistory())));

		
		return CompletableFuture.allOf(user2currentJornals.initialize(),
				user2jornalAndHistory.initialize(),
				user2money.initialize(),
				jornal2users.initialize(),
				jornal2money.initialize(),
				jornal2userAndHistory.initialize()
				);
	}
	
	private List<String> History2csvTuple(Map<String, List<Boolean>> history){
		List<String> list = new ArrayList<>();
		list.add("exists");
		list.addAll(history.entrySet()
				.stream()
				.map(e->e.getKey()+","+e.getValue()
						.stream()
						.map(v->v?"1":"0")
						.reduce("", (a,b)-> a+b))
				.collect(Collectors.toList()));
		return list;
	}
}
