package il.ac.technion.cs.sd.sub.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.google.inject.Inject;

import net.bytebuddy.description.annotation.AnnotationList.Empty;
import storage.Storages.FutureStorage;
import storage.Storages.StringStorages.LibManager;

public class SubscriberReaderImpl implements SubscriberReader {
	private static final String JORNAL2USER_AND_HISTORY = "jornal2userAndHistory";
	private static final String JORNAL2MONEY = "jornal2money";
	private static final String JORNAL2USERS = "jornal2users";
	private static final String USER2MONEY = "user2money";
	private static final String USER2JORNAL_AND_HISTORY = "user2jornalAndHistory";
	private static final String USER2ALL_TIMES_CANCELED_JORNALS = "user2allTimesCanceledJornals";
	private static final String USER2CURRENT_CANCELED_JORNALS = "user2currentCanceledJornals";
	private static final String USER2ALL_TIMES_JORNALS = "user2allTimesJornals";
	private static final String USER2CURRENT_JORNALS = "user2currentJornals";

	FutureStorage<String> user2currentJornals;
	FutureStorage<String> user2allTimesJornals;
	FutureStorage<String> user2currentCanceledJornals;
	FutureStorage<String> user2allTimesCanceledJornals;
	FutureStorage<String> user2jornalAndHistory;
	FutureStorage<String> user2money;

	FutureStorage<String> jornal2users;
	FutureStorage<String> jornal2money;
	FutureStorage<String> jornal2userAndHistory;

	@Inject
	public SubscriberReaderImpl(LibManager lib) {
		user2currentJornals = lib.get(USER2CURRENT_JORNALS);
		user2allTimesJornals = lib.get(USER2ALL_TIMES_JORNALS);
		user2currentCanceledJornals = lib.get(USER2CURRENT_CANCELED_JORNALS);
		user2allTimesCanceledJornals = lib.get(USER2ALL_TIMES_CANCELED_JORNALS);
		user2jornalAndHistory = lib.get(USER2JORNAL_AND_HISTORY);
		user2money = lib.get(USER2MONEY);

		jornal2users = lib.get(JORNAL2USERS);
		jornal2money = lib.get(JORNAL2MONEY);
		jornal2userAndHistory = lib.get(JORNAL2USER_AND_HISTORY);
	}

	@Override
	public CompletableFuture<Optional<Boolean>> isSubscribed(String userId, String journalId) {
		CompletableFuture<Optional<Map<String, List<Boolean>>>> history = getHistory(userId);
		return history.thenApply(v -> {
			if (!v.isPresent()) {
				return Optional.empty();
			}
			if (!v.get().containsKey(journalId)) {
				return Optional.of(false);
			} else {
				List<Boolean> user2joranlHistory = v.get().get(journalId);
				return Optional.of(user2joranlHistory.get(user2joranlHistory.size() - 1));
			}
		});
	}

	@Override
	public CompletableFuture<Optional<Boolean>> wasSubscribed(String userId, String journalId) {
		CompletableFuture<Optional<Map<String, List<Boolean>>>> history = getHistory(userId);
		return history.thenApply(v -> {
			if (!v.isPresent()) {
				return Optional.empty();
			}
			if (!v.get().containsKey(journalId)) {
				return Optional.of(false);
			} else {
				List<Boolean> user2joranlHistory = v.get().get(journalId);
				return Optional.of(user2joranlHistory.contains(true));
			}
		});
	}

	@Override
	public CompletableFuture<Optional<Boolean>> isCanceled(String userId, String journalId) {
		CompletableFuture<Optional<Map<String, List<Boolean>>>> history = getHistory(userId);
		return history.thenApply(v -> {
			if (!v.isPresent()) {
				return Optional.empty();
			}
			if (!v.get().containsKey(journalId)) {
				return Optional.of(false);
			} else {
				List<Boolean> user2joranlHistory = v.get().get(journalId);
				return Optional.of(!user2joranlHistory.get(user2joranlHistory.size() - 1) && user2joranlHistory.contains(true));
			}
		});
	}

	@Override
	public CompletableFuture<Optional<Boolean>> wasCanceled(String userId, String journalId) {
		CompletableFuture<Optional<Map<String, List<Boolean>>>> history = getHistory(userId);
		return history.thenApply(v -> {
			if (!v.isPresent()) {
				return Optional.empty();
			}
			if (!v.get().containsKey(journalId)) {
				return Optional.of(false);
			} else {
				List<Boolean> user2joranlHistory = v.get().get(journalId);
				return Optional.of(wasCanceled(user2joranlHistory));
			}
		});
	}

	private boolean wasCanceled(List<Boolean> user2joranlHistory) {
		int lastTimeCanceled = user2joranlHistory.lastIndexOf(false);
		int firstTimeSubscribe = user2joranlHistory.indexOf(true);
		if (lastTimeCanceled < 0 || firstTimeSubscribe < 0)
			return false;
		return (firstTimeSubscribe < lastTimeCanceled);
	}

	@Override
	public CompletableFuture<List<String>> getSubscribedJournals(String userId) {
		return user2currentJornals.getValuesOf(userId)
				.thenApply(v -> v.stream().map(x -> x.trim()).sorted().collect(Collectors.toList()));
	}

	@Override
	public CompletableFuture<Map<String, List<Boolean>>> getAllSubscriptions(String userId) {
		return getHistory(userId).thenApply(v -> v.isPresent() ? v.get() : new HashMap<>());
	}

	private CompletableFuture<Optional<Map<String, List<Boolean>>>> getHistory(String userId) {
		return user2jornalAndHistory.getValuesOf(userId).thenApply(x -> {
			if (x.isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(x.stream().skip(1).map(v -> v.trim().split(","))
						.collect(Collectors.toMap(v -> v[0].trim(), v -> binary2List(v[1].trim()))));
			}
		});
	}

	private List<Boolean> binary2List(String binary) {
		return binary.chars().mapToObj(v -> (char) v).map(v -> {
			if (v.charValue() == '0')
				return false;
			else
				return true;
		}).collect(Collectors.toList());
	}

	@Override
	public CompletableFuture<OptionalInt> getMonthlyBudget(String userId) {
		return user2money.getFirstValueOf(userId).thenApply(v -> {
			if (v.isPresent()) {
				return OptionalInt.of(Integer.valueOf(v.get().trim()));
			} else {
				return OptionalInt.empty();
			}
		});
	}

	@Override
	public CompletableFuture<List<String>> getSubscribedUsers(String journalId) {
		return jornal2users.getValuesOf(journalId)
				.thenApply(v -> v.stream().map(x -> x.trim()).sorted().collect(Collectors.toList()));
	}

	@Override
	public CompletableFuture<OptionalInt> getMonthlyIncome(String journalId) {
		return jornal2money.getFirstValueOf(journalId).thenApply(v -> {
			if (v.isPresent()) {
				return OptionalInt.of(Integer.valueOf(v.get().trim()));
			} else {
				return OptionalInt.empty();
			}
		});
	}

	@Override
	public CompletableFuture<Map<String, List<Boolean>>> getSubscribers(String journalId) {
		return jornal2userAndHistory.getValuesOf(journalId)
				.thenApply(x -> x.stream().skip(1).map(v -> v.trim().split(","))
						.collect(Collectors.toMap(v -> v[0].trim(), v -> binary2List(v[1].trim()))));
	}

}
