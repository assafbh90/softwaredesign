package storage.Fakes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import il.ac.technion.cs.sd.sub.ext.FutureLineStorage;
import il.ac.technion.cs.sd.sub.ext.FutureLineStorageFactory;

public class FactoryLineStorageFake implements FutureLineStorageFactory {
	Map<String, FutureLineStorage> storages = new HashMap<>();
	@Override
	public CompletableFuture<Optional<FutureLineStorage>> open(String arg0) {
		FutureLineStorage storage;
		if(storages.containsKey(arg0)){
			storage = storages.get(arg0);
		} else {
			storage = new LineStorageFake();
			storages.put(arg0, storage);
		}
		return CompletableFuture.completedFuture(Optional.of(storage));
	}

}
