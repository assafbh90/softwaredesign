package storage.Fakes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.CompletableFuture;
import il.ac.technion.cs.sd.sub.ext.FutureLineStorage;


public class LineStorageFake implements FutureLineStorage {
	List<String> storage = new ArrayList<String>();

	@Override
	public CompletableFuture<Boolean> appendLine(String s) {
		return CompletableFuture.supplyAsync(() -> {
			storage.add(s);
			return true;
		});
	}

	@Override
	public CompletableFuture<Optional<String>> read(int lineNumber) {
		return CompletableFuture.supplyAsync(() -> {
			String s = storage.get(lineNumber);
			try {
				Thread.sleep(s.length());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return Optional.of(s);
		});
	}

	@Override
	public CompletableFuture<OptionalInt> numberOfLines() {
		return CompletableFuture.supplyAsync(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return OptionalInt.of(storage.size());
		});
	}

}
