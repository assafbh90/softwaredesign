package storage.Storages.StringStorages;

import com.google.inject.Inject;

import storage.StoragePrivate.Loaders.LoaderFactory;
import storage.StoragePrivate.Readers.ReaderFactory;
import storage.Storages.FutureStorage;

public class StorageFactoryImpl implements StorageFactory<String> {
	LoaderFactory<String> loader;
	ReaderFactory<String> reader;
	
	@Inject
	StorageFactoryImpl(LoaderFactory<String> loader, ReaderFactory<String> reader) {
		this.loader = loader;
		this.reader = reader;
	}
	@Override
	public FutureStorage<String> create(String name) {
		return new SortedFutureStorage(loader, reader, name);
	}
}
