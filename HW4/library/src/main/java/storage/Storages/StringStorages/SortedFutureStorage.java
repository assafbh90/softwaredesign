package storage.Storages.StringStorages;

import storage.StoragePrivate.Loaders.FutureStorageLoader;
import storage.StoragePrivate.Loaders.LoaderFactory;
import storage.StoragePrivate.Readers.FutureStorageReader;
import storage.StoragePrivate.Readers.ReaderFactory;
import storage.Storages.FutureStorage;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

//no need testing, only wrapper class.
public class SortedFutureStorage implements FutureStorage<String> {
    private final FutureStorageLoader<String> storageLoader;
    private final FutureStorageReader<String> storageReader;


    SortedFutureStorage(LoaderFactory<String> loader, ReaderFactory<String> reader, String name) {
        this.storageLoader = loader.create(name);
        this.storageReader = reader.create(name);
    }

    @Override
    public void loadData(String key, String value) {
        storageLoader.loadData(key,value);
    }

    @Override
    public void loadData(String key, List<String> values) {
        storageLoader.loadData(key,values);
    }

    @Override
    public CompletableFuture<Void> initialize() {
        return storageLoader.initialize();
    }

    @Override
    public CompletableFuture<List<String>> getValuesOf(String key) {
        return storageReader.getValuesOf(key).thenApply(this::cleanList);
    }
	private List<String> cleanList(List<String> list) {
		return list.stream().map(v->v.replace(" ", "")).filter(v->!v.equals("")).collect(Collectors.toList());
	}
    @Override
    public CompletableFuture<Optional<String>> getFirstValueOf(final String key) {
        return storageReader.getFirstValueOf(key);
    }
}
