package storage.Storages.StringStorages;

import storage.Storages.FutureStorage;

public interface LibManager {
	FutureStorage<String> add(String name);
	FutureStorage<String> get(String name);
}
