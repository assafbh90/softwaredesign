package storage.Storages.StringStorages;

import storage.Storages.FutureStorage;

public interface StorageFactory<T> {
	FutureStorage<T> create(String name);
}
