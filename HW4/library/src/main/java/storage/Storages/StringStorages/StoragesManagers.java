package storage.Storages.StringStorages;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;

import storage.Storages.FutureStorage;

public class StoragesManagers implements LibManager {
	Map<String,FutureStorage<String>> tables;
	StorageFactory<String> storageFactory;
	
	@Inject
	StoragesManagers(StorageFactory<String> storageFactory) {
		tables = new HashMap<>();
		this.storageFactory = storageFactory;
	}
	
	public FutureStorage<String> add(String name){
		FutureStorage<String> storage = storageFactory.create(name);
		tables.put(name, storage);
		return storage;
	}
	public FutureStorage<String> get(String name){
		return tables.get(name);
	}
}
