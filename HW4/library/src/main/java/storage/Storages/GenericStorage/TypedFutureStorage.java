package storage.Storages.GenericStorage;

import com.google.inject.Inject;
import storage.Storages.FutureStorage;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;


public class TypedFutureStorage<T,S extends Storable<T>> implements FutureStorage<T> {

    private final FutureStorage<String> storage;
    private final S restoreFactory;

    @Inject
    public TypedFutureStorage(final FutureStorage<String> storage,
                              final S restoreFactory) {
        this.storage = storage;
        this.restoreFactory = restoreFactory;
    }

    @Override
    public void loadData(String key, T value) {
        storage.loadData(key, restoreFactory.convertToString(value));
    }

    @Override
    public void loadData(String key, List<T> values) {
        List<String> strValues = values.stream()
                .map(restoreFactory::convertToString)
                .collect(Collectors.toList());

        storage.loadData(key, strValues);
    }

    @Override
    public CompletableFuture<Void> initialize() {
        return storage.initialize();
    }

    @Override
    public CompletableFuture<List<T>> getValuesOf(String key) {

        return storage.getValuesOf(key).
                thenApply((list) -> list.stream()
                    .map(restoreFactory::convertFromString)
                    .collect(Collectors.toList()));
    }

    @Override
    public CompletableFuture<Optional<T>> getFirstValueOf(final String key) {
        return storage.getFirstValueOf(key)
                .thenApply(v->v.map(restoreFactory::convertFromString));
    }
}

