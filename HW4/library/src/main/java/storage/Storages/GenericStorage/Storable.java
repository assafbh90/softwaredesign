package storage.Storages.GenericStorage;


public interface Storable<T> {
    String convertToString(T value);
    T convertFromString(String value);
}
