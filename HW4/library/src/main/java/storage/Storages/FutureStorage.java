package storage.Storages;

import storage.StoragePrivate.Loaders.FutureStorageLoader;
import storage.StoragePrivate.Readers.FutureStorageReader;


public interface FutureStorage<T> extends FutureStorageLoader<T>,FutureStorageReader<T> {
}
