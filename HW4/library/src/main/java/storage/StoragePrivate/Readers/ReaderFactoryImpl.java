package storage.StoragePrivate.Readers;

import com.google.inject.Inject;

import storage.Managers.FutureStorageManagerFactory;

public class ReaderFactoryImpl implements ReaderFactory<String> {
	private FutureStorageManagerFactory storgaeFactory;
	
	@Inject
	ReaderFactoryImpl(FutureStorageManagerFactory factory) {
		this.storgaeFactory = factory;
	}
	@Override
	public FutureStorageReader<String> create(String name) {
		return new SortedStorageReader(storgaeFactory.create(name));
	}
}
