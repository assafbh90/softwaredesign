package storage.StoragePrivate.Readers;

import storage.StoragePrivate.Loaders.FutureStorageLoader;

public interface ReaderFactory<T> {
	FutureStorageReader<T> create(String name);
}
