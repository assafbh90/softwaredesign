package storage.StoragePrivate.Readers;

import storage.Managers.FutureStorageManager;
import storage.StoragePrivate.Items.StorageValue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;


public class SortedStorageReader implements FutureStorageReader<String> {

    private final FutureStorageManager futureStorageManager;

    public SortedStorageReader(final FutureStorageManager futureStorageManager) {
        this.futureStorageManager = futureStorageManager;
    }
	private final char EscapeChar = 14;

	private String unescape(String s) {
		return s.replace(EscapeChar, ',');
	}

    @Override
    public CompletableFuture<List<String>> getValuesOf(String key) {
        return binarySearch(key);
    }

    @Override
    public CompletableFuture<Optional<String>> getFirstValueOf(final String key) {
        return getValuesOf(key)
                .thenApply(this::getFirstValue);
    }


    private Optional<String> getFirstValue(final List<String> v) {
        if (v.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(v.get(0));
        }
    }


    private CompletableFuture<List<String>> binarySearch(String key) {
        return futureStorageManager.getSize()
                .thenCompose(storageSize ->
                        doBinarySearch(key, 0, storageSize - 1));
    }

    private CompletableFuture<List<String>> doBinarySearch(String key, int startIndex, int endIndex) {
        if (startIndex > endIndex) {
            return CompletableFuture.completedFuture(Collections.emptyList());
        }
        int curIndex = startIndex + (endIndex - startIndex) / 2;
        return futureStorageManager.readKey(curIndex)
                .thenCompose(readKey -> {
                    if (readKey.compareTo(key) < 0) {
                        return doBinarySearch(key, curIndex + 1, endIndex);
                    } else if (readKey.compareTo(key) > 0) {
                        return doBinarySearch(key, startIndex, curIndex - 1);
                    }
                    return futureStorageManager.readValue(curIndex).thenApply(StorageValue::getValues).thenApply(v->v.stream().map(this::unescape).collect(Collectors.toList()));
                });
    }
	
}


