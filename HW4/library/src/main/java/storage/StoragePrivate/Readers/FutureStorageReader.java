package storage.StoragePrivate.Readers;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;


public interface FutureStorageReader<T> {
    CompletableFuture<List<T>> getValuesOf(String key);
    CompletableFuture<Optional<T>> getFirstValueOf(String key);
}
