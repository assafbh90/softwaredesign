package storage.StoragePrivate.Loaders;

import storage.Managers.FutureStorageManager;
import storage.StoragePrivate.Items.StorageValue;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;


public class SortedFutureStorageLoader implements FutureStorageLoader<String> {
    private final Map<String, List<String>> initMap = new TreeMap<>();
    private final FutureStorageManager futureStorageManager;

    public SortedFutureStorageLoader(FutureStorageManager futureStorageManager) {
        this.futureStorageManager = futureStorageManager;
    }

    @Override
    public final void loadData(final String key,
                               final String value) {

        loadData(key, Collections.singletonList(value));

    }

    @Override
    public final void loadData(final String key,
                               final List<String> values) {

        initMap.computeIfAbsent(key,
                (v) -> new ArrayList<>())
                .addAll(values);

    }

    @Override
    public CompletableFuture<Void> initialize() {
    	CompletableFuture<Void> tasks = CompletableFuture.completedFuture(null);
    	for(Entry<String, List<String>> e : initMap.entrySet()){
    		tasks = tasks.thenCompose(v->storeData(e.getKey(),new StorageValue(e.getValue())));
    	}
    	return tasks;
    }

    List<String> loadedValues(final String key) {
        return initMap.getOrDefault(key, Collections.emptyList());

    }


    private CompletableFuture<Void> storeData(final String key, final StorageValue item) {
           return futureStorageManager.add(key,item);
    }
}
