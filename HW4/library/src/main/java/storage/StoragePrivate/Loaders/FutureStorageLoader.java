package storage.StoragePrivate.Loaders;


import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface FutureStorageLoader<T> {
    void loadData(String key, T value);
    void loadData(String key, List<T> values);
    CompletableFuture<Void> initialize();
}
