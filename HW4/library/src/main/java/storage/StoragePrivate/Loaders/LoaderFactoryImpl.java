package storage.StoragePrivate.Loaders;

import com.google.inject.Inject;

import storage.Managers.FutureStorageManagerFactory;

public class LoaderFactoryImpl implements LoaderFactory<String>{
	private FutureStorageManagerFactory storgaeFactory;
	
	@Inject
	LoaderFactoryImpl(FutureStorageManagerFactory factory) {
		this.storgaeFactory = factory;
	}
	@Override
	public FutureStorageLoader<String> create(String name) {
		return new SortedFutureStorageLoader(storgaeFactory.create(name));
	}

}
