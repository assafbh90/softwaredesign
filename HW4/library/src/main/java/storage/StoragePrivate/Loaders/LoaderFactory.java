package storage.StoragePrivate.Loaders;

public interface LoaderFactory<T> {

	FutureStorageLoader<T> create(String name);
}
