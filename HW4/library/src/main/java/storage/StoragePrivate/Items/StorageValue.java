package storage.StoragePrivate.Items;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StorageValue {
    private final List<String> values;

    private static final String valuesSeparator = ",";

    public StorageValue(final List<String> values) {
        this.values = values.stream().map(this::escape).collect(Collectors.toList());
    }
    
	private final char EscapeChar = 14;

	private String escape(String s) {
		return s.replace(',', EscapeChar);
	}

	private String unescape(String s) {
		return s.replace(EscapeChar, ',');
	}


    public StorageValue(final String line){
        this(Arrays.asList(line.split(valuesSeparator))); //values don't change so we can use asList
    }

    @Override
    public String toString(){
        return FormattedList();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof StorageValue && ((StorageValue) o).getValues().equals(getValues());
    }

    public final List<String> getValues(){
        return values.stream().map(this::unescape).collect(Collectors.toList());
    }

    private String FormattedList(){
        String valuesStringFormat = values.toString();
        valuesStringFormat = valuesStringFormat.substring(1,valuesStringFormat.length()-1);
        return valuesStringFormat;
    }

}
