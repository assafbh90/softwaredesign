package storage.Managers;

import storage.StoragePrivate.Items.StorageValue;

import java.util.concurrent.CompletableFuture;


public interface FutureStorageManager {
    CompletableFuture<Void> add(String key, final StorageValue values);
    CompletableFuture<String> readKey(int index);
    CompletableFuture<StorageValue> readValue(int index);
    CompletableFuture<Integer> getSize();
}
