package storage.Managers;

import javax.inject.Inject;

import il.ac.technion.cs.sd.sub.ext.FutureLineStorageFactory;

public class FutureLineStorageManagerFactory implements FutureStorageManagerFactory {
	private FutureLineStorageFactory factory;
	
	@Inject
	public FutureLineStorageManagerFactory(final FutureLineStorageFactory factory) {
		this.factory = factory;
	}

	@Override
	public FutureStorageManager create(String storageName) {
		return new FutureLineStorageManager(factory, storageName);
	}
	
}
