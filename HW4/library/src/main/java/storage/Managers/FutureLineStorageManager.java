package storage.Managers;

import il.ac.technion.cs.sd.sub.ext.FutureLineStorage;
import il.ac.technion.cs.sd.sub.ext.FutureLineStorageFactory;
import storage.StoragePrivate.Items.StorageValue;

import javax.inject.Inject;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class FutureLineStorageManager implements FutureStorageManager {

	private final CompletableFuture<FutureLineStorage> keyStorage;
	private final CompletableFuture<FutureLineStorage> valueStorage;

	public FutureLineStorageManager(final FutureLineStorageFactory factory, final String storageName) {
		this.keyStorage = open(factory, storageName + "_KEY");
		this.valueStorage = open(factory, storageName + "_VALUE");
	}

	private CompletableFuture<FutureLineStorage> open(final FutureLineStorageFactory factory,
			final String storageName) {
		return factory.open(storageName).thenCompose(v -> {
			if (v.isPresent()) {
				return CompletableFuture.completedFuture(v.get());
			} else {
				return open(factory, storageName);
			}
		});
	}

	@Override
	public CompletableFuture<Void> add(final String key, final StorageValue values) {
		return CompletableFuture.allOf(keyStorage.thenCompose(ks -> appendLine(ks, key)),
				valueStorage.thenCompose((vs) -> appendLine(vs, values.toString())));
	}

	private CompletableFuture<Void> appendLine(FutureLineStorage storage, final String key) {
		return storage.appendLine(key).thenCompose(bool -> {
			if (bool) {
				return CompletableFuture.completedFuture(null);
			} else {
				return appendLine(storage, key);
			}
		});
	}

	@Override
	public CompletableFuture<String> readKey(int index) {
		return keyStorage.thenCompose((ks) -> read(ks, index));
	}
	
	@Override
	public CompletableFuture<StorageValue> readValue(int index) {
		return valueStorage.thenCompose((vs) -> read(vs, index)).thenApply(StorageValue::new);
	}
	
	private CompletableFuture<String> read(FutureLineStorage storage, int index) {
		return storage.read(index).thenCompose(optional -> {
			if (optional.isPresent()) {
				return CompletableFuture.completedFuture(optional.get());
			} else {
				return read(storage, index);
			}
		});
	}

	@Override
	public CompletableFuture<Integer> getSize() {
		return keyStorage.thenCompose(ks->getSize(ks));
	}
	public CompletableFuture<Integer> getSize(FutureLineStorage storage){
		return storage.numberOfLines().thenCompose(optional->{
			if (optional.isPresent()) {
				return CompletableFuture.completedFuture(optional.getAsInt());
			} else {
				return getSize(storage);
			}
		});
	}
}