package storage.Managers;

public interface FutureStorageManagerFactory {
	FutureStorageManager create(final String storageName);
}
