package guice;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import il.ac.technion.cs.sd.sub.ext.FutureLineStorageFactory;
import il.ac.technion.cs.sd.sub.ext.LineStorageModule;
import storage.Fakes.FactoryLineStorageFake;
import storage.Managers.FutureLineStorageManager;
import storage.Managers.FutureLineStorageManagerFactory;
import storage.Managers.FutureStorageManager;
import storage.Managers.FutureStorageManagerFactory;
import storage.StoragePrivate.Loaders.FutureStorageLoader;
import storage.StoragePrivate.Loaders.LoaderFactory;
import storage.StoragePrivate.Loaders.LoaderFactoryImpl;
import storage.StoragePrivate.Loaders.SortedFutureStorageLoader;
import storage.StoragePrivate.Readers.FutureStorageReader;
import storage.StoragePrivate.Readers.ReaderFactory;
import storage.StoragePrivate.Readers.ReaderFactoryImpl;
import storage.StoragePrivate.Readers.SortedStorageReader;
import storage.Storages.FutureStorage;
import storage.Storages.StringStorages.LibManager;
import storage.Storages.StringStorages.SortedFutureStorage;
import storage.Storages.StringStorages.StorageFactory;
import storage.Storages.StringStorages.StorageFactoryImpl;
import storage.Storages.StringStorages.StoragesManagers;

public class libModule extends AbstractModule {

	@Override
	protected void configure() {
		//install(new LineStorageModule());
		bind(FutureLineStorageFactory.class).toInstance(new FactoryLineStorageFake());
		bind(FutureStorageManagerFactory.class).to(FutureLineStorageManagerFactory.class);
		bind(new TypeLiteral<LoaderFactory<String>>(){}).to(LoaderFactoryImpl.class);
		bind(new TypeLiteral<ReaderFactory<String>>(){}).to(ReaderFactoryImpl.class);
		bind(new TypeLiteral<StorageFactory<String>>(){}).to(StorageFactoryImpl.class);
		bind(LibManager.class).to(StoragesManagers.class);
	}


}
