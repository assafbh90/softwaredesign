package storage.StoragePrivate.Loaders;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import storage.Managers.FutureStorageManager;
import storage.StoragePrivate.Items.StorageValue;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;

public class SortedStorageLoaderTest {

    private final String KEY1 = "1";
    private final String KEY2 = "2";
    private final String VALUE1 = "a";
    private final String VALUE2 = "b";
    private final String VALUE3 = "c";
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    SortedFutureStorageLoader storageLoader;
    @Mock
    private FutureStorageManager mockManager;

    @Before
    public void setUp() {
        storageLoader = new SortedFutureStorageLoader(mockManager);
    }

    @Test
    public void shouldAddMultipleValuesToKey() {
        storageLoader.loadData(KEY1, Arrays.asList(VALUE1, VALUE2, VALUE3));

        assertEquals(Arrays.asList(VALUE1, VALUE2, VALUE3), storageLoader.loadedValues(KEY1));
    }

    @Test
    public void shouldAddValueByKey() {
        storageLoader.loadData(KEY1, VALUE1);
        storageLoader.loadData(KEY2, VALUE2);

        assertEquals(Arrays.asList(VALUE1), storageLoader.loadedValues(KEY1));
        assertEquals(Arrays.asList(VALUE2), storageLoader.loadedValues(KEY2));
    }

    @Test
    public void shouldInitializeAllItems() throws Exception {
        Mockito.when(mockManager.add(Mockito.anyString(), Mockito.any(StorageValue.class)))
                .thenReturn(CompletableFuture.completedFuture(null));
        storageLoader.loadData(KEY1, Arrays.asList(VALUE1,VALUE3));
        storageLoader.loadData(KEY2, VALUE2);

        storageLoader.initialize().get();

        Mockito.verify(mockManager).add(KEY1,new StorageValue(Arrays.asList(VALUE1,VALUE3)));
        Mockito.verify(mockManager).add(KEY2,new StorageValue(Arrays.asList(VALUE2)));
    }

}