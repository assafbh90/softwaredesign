package storage.StoragePrivate.Items;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class StorageValueTest {

    private final List<String> ARRAY_OF_VALUES1 = Arrays.asList("a","b","c");
    private final List<String> ARRAY_OF_VALUES2 = Arrays.asList("a");


    @Test
    public void canParseValues(){
        StorageValue item = new StorageValue(ARRAY_OF_VALUES1);

        StorageValue readLineItem = new StorageValue(item.toString().replace(" ", ""));

        assertEquals(ARRAY_OF_VALUES1,readLineItem.getValues());
    }

    @Test
    public void shouldBeEqualWhenReadingSameValue(){
        StorageValue firstItem = new StorageValue(ARRAY_OF_VALUES1);
        StorageValue secondItem = new StorageValue(ARRAY_OF_VALUES1);

        assertEquals(firstItem,secondItem);
    }

    @Test
    public void shouldNotBeEqualWhenReadingDifferentValue(){
        StorageValue firstItem = new StorageValue(ARRAY_OF_VALUES1);
        StorageValue secondItem = new StorageValue(ARRAY_OF_VALUES2);

        assertNotEquals(firstItem,secondItem);
    }

    @Test
    public void shouldHandleValuesParsing(){
        StorageValue firstItem = new StorageValue(ARRAY_OF_VALUES1);

        String asString = firstItem.toString();

        assertEquals(firstItem,new StorageValue(asString.replace(" ", "")));
    }

}