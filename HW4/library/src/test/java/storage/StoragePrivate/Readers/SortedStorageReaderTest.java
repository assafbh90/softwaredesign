package storage.StoragePrivate.Readers;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import storage.Managers.FutureStorageManager;
import storage.StoragePrivate.Items.StorageValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


public class SortedStorageReaderTest {
    private final static int MANY_LINES = 1000000;
    private final static int FEW_LINES = 10;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private FutureStorageManager mockFutureStorageManager;
    private FutureStorageReader<String> storageReader;

    @Before
    public void setUp() throws Exception {
        storageReader = new SortedStorageReader(mockFutureStorageManager);
        when(mockFutureStorageManager.readKey(anyInt()))
                .thenAnswer(args -> CompletableFuture.completedFuture(mockedKey(args.getArgument(0))));
        when(mockFutureStorageManager.readValue(anyInt()))
                .thenAnswer(args -> CompletableFuture.completedFuture(new StorageValue(mockedValue(args.getArgument(0)))));
    }

    @Test
    public void returnsEmptyListWhenEmptyStorage() throws Exception {
        testGetValueMethod(mockedKey(0), 0, Collections.EMPTY_LIST);
    }

    @Test
    public void returnsCorrectValueInSmallStorage() throws Exception {
        int lineNum = randomLineNum(FEW_LINES);
        testGetValueMethod(mockedKey(lineNum), FEW_LINES, mockedValue(lineNum));
    }

    @Test
    public void returnsLastValueInSmallStorage() throws Exception {
        int indexNum = FEW_LINES - 1;
        testGetValueMethod(mockedKey(indexNum), FEW_LINES, mockedValue(indexNum));
    }

    @Test
    public void returnsFirstItemValueInSmallStorage() throws Exception {
        int lineNum = 0;
        testGetValueMethod(mockedKey(lineNum), FEW_LINES, mockedValue(lineNum));
    }

    @Test
    public void returnsCorrectItemInLargeStorage() throws Exception {
        int lineNum = randomLineNum(MANY_LINES);
        testGetValueMethod(mockedKey(lineNum), MANY_LINES, mockedValue(lineNum));
    }

    @Test
    public void returnsLastItemInLargeStorage() throws Exception {
        int lineNum = MANY_LINES - 1;
        testGetValueMethod(mockedKey(lineNum), MANY_LINES, mockedValue(lineNum));

    }

    @Test
    public void returnsFirstItemInLargeStorage() throws Exception {
        int lineNum = 0;
        testGetValueMethod(mockedKey(lineNum), MANY_LINES, mockedValue(lineNum));
    }

    @Test
    public void returnsEmptyListWhenKeyNotExists() throws Exception {
        testGetValueMethod(mockedKey(FEW_LINES + 1), FEW_LINES, Collections.EMPTY_LIST);
    }

    @Test
    public void doesNotReadMoreThenLogNKeys() {
        when(mockFutureStorageManager.getSize()).thenReturn(CompletableFuture.completedFuture(MANY_LINES));

        int lineNum = MANY_LINES - 1;
        storageReader.getValuesOf(mockedKey(lineNum));

        verify(mockFutureStorageManager, atMost((int) log2(MANY_LINES) + 1)).readKey(anyInt());
    }

    @Test
    public void shouldReadExactlyOneValue() {
        when(mockFutureStorageManager.getSize()).thenReturn(CompletableFuture.completedFuture(MANY_LINES));

        int lineNum = MANY_LINES - 1;
        storageReader.getValuesOf(mockedKey(lineNum));

        verify(mockFutureStorageManager, times(1)).readValue(anyInt());
    }

    private void testGetValueMethod(String key, int storageSize, List<String> expectedResult) throws Exception {
        when(mockFutureStorageManager.getSize()).thenReturn(CompletableFuture.completedFuture(storageSize));

        List<String> value = storageReader.getValuesOf(key).get();

        assertEquals(expectedResult, value);
    }





    private static String mockedKey(int i) {
        return "key_" + mockedFormat(i);
    }

    private static List<String> mockedValue(int i) {
        return Arrays.asList("value1_" + mockedFormat(i),"value2_" + mockedFormat(i));
    }

    private static String mockedFormat(int i) {
        return String.format("%50s", Integer.toString(i)).replace(' ', '0');
    }

    private static int randomLineNum(int numOfLines) {
        return new Random().nextInt(numOfLines);
    }

    private static double log2(int x) {
        return Math.log(x) / Math.log(2);
    }
}