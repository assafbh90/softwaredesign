package storage.Storages.GenericStorage;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import storage.Storages.FutureStorage;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TypedStorageTest {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock private FutureStorage<String> storage;

    private TypedFutureStorage<StorableMock, StorableMockFactory> typedStorage;
    private final String KEY1 = "a";
    private final Integer VALUE1 = 1;
    private final Integer VALUE2 = 2;
    private final Integer VALUE3 = 3;
    private final Integer VALUE4 = 4;
    private final StorableMock storableMock1 = new StorableMock(VALUE1,VALUE2);
    private final StorableMock storableMock2 = new StorableMock(VALUE3,VALUE4);
    private final StorableMockFactory storableMockFactory= new StorableMockFactory();

    @Before
    public void setUp(){
        typedStorage = new TypedFutureStorage<>(storage, storableMockFactory);
    }

    @Test
    public void shouldInsertToStorageWithCorrectStringValue() {
        typedStorage.loadData(KEY1,storableMock1);

        verify(storage).loadData(KEY1,storableMockFactory.convertToString(storableMock1));
    }

    @Test
    public void shouldInsertToStorageWithCorrectStringValues() {
        typedStorage.loadData(KEY1, Arrays.asList(storableMock1,storableMock2));

        verify(storage).loadData(KEY1,Arrays.asList(storableMockFactory.convertToString(storableMock1),
                storableMockFactory.convertToString(storableMock2)));
    }

    @Test
    public void shouldInitializeStringStorage() throws Exception {
        when(storage.initialize()).thenReturn(CompletableFuture.completedFuture(null));
        typedStorage.initialize().get();

        verify(storage).initialize();
    }

    @Test
    public void shouldReturnCorrectTypedValues() throws Exception {
        when(storage.getValuesOf(KEY1)).thenReturn(CompletableFuture.completedFuture(
                Arrays.asList(storableMockFactory.convertToString(storableMock1),
                        storableMockFactory.convertToString(storableMock2))));

        List<StorableMock> valuesList = typedStorage.getValuesOf(KEY1).get();

        assertEquals(Arrays.asList(storableMock1,storableMock2), valuesList);
    }

}

class StorableMock {
    private final int i;
    private final int j;

    StorableMock(int i, int j){
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    @Override
    public boolean equals(Object o){
        return o instanceof StorableMock && ((StorableMock) o).getI() == getI() && ((StorableMock) o).getJ() == getJ();
    }
}

class StorableMockFactory implements Storable<StorableMock>{
    @Override
    public String convertToString(final StorableMock value) {
        return value.getI()+":"+value.getJ();
    }

    @Override
    public StorableMock convertFromString(final String value) {
        return new StorableMock(Integer.parseInt(value.split(":")[0]),
                Integer.parseInt(value.split(":")[1]));
    }

}
