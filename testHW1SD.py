import random
import string
import sys

Reviewers = dict()
Books = dict()

def addReview(f):
    ReviewId = random.randint(0, len(Books) - 1)
    Books[list(Books)[ReviewId]] += 1

    write2file(f, '\t\t<Review>\n')
    write2file(f, '\t\t\t<Id>%s</Id>\n' % list(Books)[ReviewId])
    write2file(f, '\t\t\t<Score>%s</Score>\n' % str(random.randint(0, 10)))
    write2file(f, '\t\t</Review>\n')

    if Books[list(Books)[int(ReviewId)]] >= 100:
        del Books[list(Books)[int(ReviewId)]]


def addReviewer(f):
    ReviewerId = random.randint(0, len(Reviewers) - 1)

    write2file(f, '\t<Reviewer Id="%s">\n' % list(Reviewers)[ReviewerId])


    numOfReviews = random.randint(1, 100)
    for i in range(numOfReviews):
        if len(Reviewers) <= 1 or len(Books) <= 1:
            break
        Reviewers[list(Reviewers)[ReviewerId]] += 1
        addReview(f)

    write2file(f, '\t</Reviewer>\n')

    if Reviewers[list(Reviewers)[int(ReviewerId)]] >= 100:
        del Reviewers[list(Reviewers)[int(ReviewerId)]]


def randomCharacters(num):
    name = str()
    for i in range(num):
        name += random.choice(string.digits + string.ascii_letters)
    return name


def main(fname):
    for i in range(random.randint(1, 1000000)):
        Reviewers[randomCharacters(random.randint(1, 10))] = int(0)

    for i in range(random.randint(1, 100000)):
        Books[randomCharacters(random.randint(1, 10))] = int(0)

    print("create file\n")
    with open(fname, 'w') as f:
        write2file(f, "<Root>\n")

        numOfReviewers = random.randint(1, 1000)
        for i in range(numOfReviewers):
            if len(Reviewers) <= 1 or len(Books) <= 1:
                break
            addReviewer(f)

        write2file(f, "</Root>\n", True)



file = list()
idx = 0
def write2file(f,text, check=False):
    global file
    global idx
    # f.write(text)
    file.append(text)
    idx += 1
    if idx >= 1000 or check:
        f.writelines(file)
        file = list()
        idx = 0
    # print(text)

if __name__ == "__main__":
    main("check2.txt")