package il.ac.technion.cs.sd.lib;

import java.util.Optional;

public interface Database<T> {
	Optional<T> get(String key) throws Exception;
	int size() throws Exception;
}
