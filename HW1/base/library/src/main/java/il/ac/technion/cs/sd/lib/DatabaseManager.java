package il.ac.technion.cs.sd.lib;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

class DatabaseManager <T> implements Database<T> {
	
	private Serializer<T> ser;
	private Storage storage;
	private Parser<T> parser;
	private int size;
	
	public DatabaseManager(Serializer<T> ser, List<T> objects, Storage storage) throws Exception {	
		this.ser = ser;
		this.storage = storage;
		this.parser = new Parser<T>(ser);
		Collections.reverse(objects);
		objects.stream()
		.filter(distinctByKey(ser::getKey))
		.sorted(Comparator.comparing(ser::getKey))
		.forEachOrdered(this::addLast);
		this.size = storage.numberOfLines();

	}

	@Override
	public Optional<T> get(String key) throws Exception {
		int low = 0;
        int high = size - 1;
        while (low <= high) {
            int middle = low + (high - low) / 2;
            String result = storage.read(middle);
            if (result == "") {
            	return Optional.empty();
            }
            String keyFromDB = parser.getKey(result);
            List<String> valueFromDB = parser.getValue(result);
			if (key.compareTo(keyFromDB) < 0) high = middle - 1;
            else if (key.compareTo(keyFromDB) > 0) low = middle + 1;
            else return Optional.ofNullable(ser.objectFactory(keyFromDB, valueFromDB));
        }
        return Optional.empty();
	}

	@Override
	public int size() throws Exception {
		return size;
	}

	private void addLast(T object) {
		storage.appendLine(parser.toCsv(ser.getKey(object), ser.getValue(object)));
	}
	
	private <R> Predicate<R> distinctByKey(Function<? super R, ?> keyExtractor) {
	    Map<Object,Boolean> seen = new ConcurrentHashMap<>();
	    return t -> !Optional.ofNullable(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE)).isPresent();
	}

}
