package il.ac.technion.cs.sd.lib;

import java.util.ArrayList;
import java.util.List;

class LineStorageFake implements Storage {
	List<String> storage = new ArrayList<String>();
	@Override
	public void appendLine(String s) {
		// TODO Auto-generated method stub
		storage.add(s);
	}

	@Override
	public String read(int lineNumber) throws Exception {
		// TODO Auto-generated method stub
		String s = storage.get(lineNumber);
		Thread.sleep(s.length());
		return s;
	}

	@Override
	public int numberOfLines() throws Exception {
		// TODO Auto-generated method stub
		Thread.sleep(100);
		return storage.size();
	}

}
