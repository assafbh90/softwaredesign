package il.ac.technion.cs.sd.lib;

public interface Storage {
	void appendLine(String s);   
	String read(int lineNumber) throws Exception;   
	int numberOfLines() throws Exception; 
}
