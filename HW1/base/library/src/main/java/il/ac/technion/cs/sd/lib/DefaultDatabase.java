package il.ac.technion.cs.sd.lib;

import java.util.List;
import java.util.Optional;

public class DefaultDatabase<T> implements Database<T> {
	DatabaseManager<T> dbManager;
	
	public DefaultDatabase(Serializer<T> ser, List<T> objects) throws Exception {
		dbManager = new DatabaseManager<T>(
				ser,
				objects,
				new SequenceStorage()
				);
	}
	public DefaultDatabase(Serializer<T> ser, List<T> objects,boolean isTest) throws Exception {
		Storage storage = isTest ? new LineStorageFake() : new SequenceStorage();
		dbManager = new DatabaseManager<T>(
				ser,
				objects,
				storage
				);
	}
	@Override
	public Optional<T> get(String key) throws Exception {
		return dbManager.get(key);
	}

	@Override
	public int size() throws Exception {
		return dbManager.size();
	}
	
}
