package il.ac.technion.cs.sd.lib;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser <T> {
	private static final String commaSeparator = ",";
	private final static String newLineSeparator = "\n";
	Serializer<T> ser;

	private List<String> splitFromCharacter(String string, String separator) {
		return Stream
				.of(string)
				.map(subString -> subString.split(separator))
				.flatMap(Arrays::stream)
				.collect(Collectors.toList());
	}
	
	public Parser (Serializer<T> ser) {
		this.ser = ser;
	}
	
	public String getKey(String result) {
		return result.split(commaSeparator)[0];
	}
	
	public List<String> getValue(String result) {
		return Stream.of(result.split(commaSeparator)).skip(1).collect(Collectors.toList());
	}
	
	public String toCsv(String key, List<String> value) {
		return  value.stream().reduce(key, (a,b) -> a + "," + b);
	}
	
	public List<String> parseToLines(String stringToSplit) {
		if (stringToSplit == "") return Arrays.asList();
		return splitFromCharacter(stringToSplit, newLineSeparator);
	}
	
	public List<T> Parse(String string) {
		if (string == "") return Arrays.asList();
		string = string.replaceAll("\r", "");
		return  parseToLines(string)
				.stream()
				.map(subString -> ser.objectFactory(getKey(subString),  getValue(subString)))
				.collect(Collectors.toList());
	}
}
