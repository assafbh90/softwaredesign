package il.ac.technion.cs.sd.lib;

import il.ac.technion.cs.sd.grades.ext.LineStorage;

public class SequenceStorage implements Storage {

	@Override
	public void appendLine(String s) {
		LineStorage.appendLine(s);
	}

	@Override
	public String read(int lineNumber) throws Exception {
		return LineStorage.read(lineNumber);
	}

	@Override
	public int numberOfLines() throws Exception {
		return LineStorage.numberOfLines();
	}
	 
}
