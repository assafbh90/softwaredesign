package il.ac.technion.cs.sd.lib;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import il.ac.technion.cs.sd.lib.Parser;
import il.ac.technion.cs.sd.lib.Serializer;

@RunWith(MockitoJUnitRunner.class)
public class ParserParseToLinesTests {
	@Mock
	Serializer<Student> s;
	
	private static final String LINE_SEPARATOR = System.lineSeparator();
	private void testRepeatedWordsSeperate(String word, String Separetor, int reapet) {
		String manyLines = word + String.join(word, Collections.nCopies(reapet, Separetor)) + word;
		List<String> list;
		

		list = new Parser<Student>(s).parseToLines(manyLines);
		
		assertEquals(manyLines.split(Separetor).length, list.size());
	}
	
	@Test 
	public void onlySeperators() {
		testRepeatedWordsSeperate("\n", "\n", 20);
	}
	
	@Test 
	public void oneWordLines() {
		testRepeatedWordsSeperate("test", LINE_SEPARATOR, 20);
	}
	
	@Test 
	public void twoWordLines() {
		testRepeatedWordsSeperate("test1 test2", LINE_SEPARATOR, 20);
	}
	@Test 
	public void specialCharactersLines() {
		testRepeatedWordsSeperate("! 	\r@#$%^&*()_+\b\t"+LINE_SEPARATOR, LINE_SEPARATOR, 20);
	}
	
}
