package il.ac.technion.cs.sd.app;

import java.util.List;
import java.util.Optional;

import il.ac.technion.cs.sd.lib.Database;
import il.ac.technion.cs.sd.lib.DefaultDatabase;
import il.ac.technion.cs.sd.lib.Parser;

/** This class will be instantiated once per test. */
public class GradesInitializer {

	private static Optional<Database<Student>> db;

	private boolean isTest = false;
	/**
	 * Saves the csvData persistently, so that it could be run using
	 * GradesRunner. The format of each line of the data is $id,$grade.
	 * @throws Exception - storage exception time limit
	 */
	GradesInitializer() {
	}
		
	GradesInitializer(boolean isTest) {
		this.isTest = isTest;
	}
	
	public void setup(String csvData) throws Exception {
		Parser<Student> parser = new Parser<>(new StudentSerializer());
		List<Student> students = parser.Parse(csvData);
		db = Optional.of(new DefaultDatabase<>(new StudentSerializer(), students, isTest));
	}
	
	public static Database<Student> getDatabase() {
		if (!db.isPresent()) throw new IllegalStateException("Database was not initialized");
		return db.get();
	}
}
