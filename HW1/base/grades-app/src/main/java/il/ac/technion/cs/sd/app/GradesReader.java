package il.ac.technion.cs.sd.app;

import java.util.Optional;
import java.util.OptionalInt;

import il.ac.technion.cs.sd.lib.Database;

/**
 * This class will only be instantiated after
 * {@link il.ac.technion.cs.sd.app.GradesInitializer#setup(java.lang.String) has been called}.
 */
public class GradesReader {
  /** Returns the grade associated with the ID, or empty. 
 * @throws Exception */
  public OptionalInt getGrade(String id) throws Exception {
    Database<Student> db = GradesInitializer.getDatabase();
    Optional<Student> s = db.get(id);
    return s.isPresent() ? OptionalInt.of(s.get().getGrade()) : OptionalInt.empty();
  }
}
