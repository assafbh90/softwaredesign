package il.ac.technion.cs.sd.app;

public class Student{
	
	private static final String commaSeperator = ",";
	private static final int ID = 0;
	private static final int GRADE = 1;
	
	private final String id;
	private final Integer grade;
	
	public Student(String id, Integer grade) {
		this.id = id;
		this.grade = grade;
	}
	public Student(String csvStudent) {
		if (csvStudent == "") {
			this.id = "0";
			this.grade = 0;
			return;
		}

		String[] studentFields = csvStudent.split(commaSeperator);
		this.id = studentFields[ID];
		this.grade = Integer.parseInt(studentFields[GRADE]);
	}
	
	public String getId() {
		return id;
	}
	
	public Integer getGrade() {
		return grade;
	}
	
	public String toString() {
		return getId() + commaSeperator + getGrade();
	}
	
//	public boolean equals(Student student){
//		return getId() == student.getId();
//	}
//	@Override
//	public int compareTo(Student student) {
//		if (getId() > student.getId()) {
//			return 1;
//		} 
//		if (getId() < student.getId()) {
//			return -1;
//		}
//		return 0;
//	}
}
