package il.ac.technion.cs.sd.buy.app.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Product {
	private String productId;
	private Integer price;
	private Map<String, Long> userIdsPurchase = new HashMap<String, Long>();
	private List<String> orderIds = new ArrayList<>();
	private Long purchasedAmount;

	public Product(String productId, Integer price) {
		this.productId = productId;
		this.price = price;
		this.purchasedAmount = Long.valueOf(0);
	}

	public Map<String, Long> getUserIdsPurchase() {
		return userIdsPurchase;
	}

	public void setUserIdsPurchase(Map<String, Long> userIdsPurchase) {
		this.userIdsPurchase = userIdsPurchase;
	}

	public List<String> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<String> orderIds) {
		this.orderIds = orderIds;
	}

	public Long getPurchasedAmount() {
		return purchasedAmount;
	}

	public void setPurchasedAmount(Integer purchasedAmount) {
		this.purchasedAmount = Long.valueOf(purchasedAmount);
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getProductId() {
		return productId;
	}

	public Integer getPrice() {
		return price;
	}

	public void addOrder(Order o) {
		if (o.getAmount() > 0) {
			String userId = o.getUserId();
			if (userIdsPurchase.containsKey(userId)) {
				userIdsPurchase.put(userId, userIdsPurchase.get(userId) + o.getAmount());
			} else {
				userIdsPurchase.put(userId, Long.valueOf(o.getAmount()));
			}
			purchasedAmount += o.getAmount();
		}
		orderIds.add(o.getOrderId());
	}
}
