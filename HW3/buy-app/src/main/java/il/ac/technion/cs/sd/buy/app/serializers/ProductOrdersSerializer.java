package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.List;

import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

// ProductId -> List<OrderId>
public class ProductOrdersSerializer implements DatabaseSerializer<Product> {

	@Override
	public Product objectFactory(String key, List<String> value) {
		Product product = new Product(key, -1);
		product.setOrderIds(value);
		return product;
	}

	@Override
	public String getKey(Product object) {
		return object.getProductId();
	}

	@Override
	public List<String> getValue(Product object) {
		return object.getOrderIds();
	}

}
