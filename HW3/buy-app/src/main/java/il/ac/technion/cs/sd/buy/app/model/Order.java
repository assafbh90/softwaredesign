package il.ac.technion.cs.sd.buy.app.model;

import java.util.ArrayList;
import java.util.List;

public class Order {
	private String orderId;
	private String userId;
	private Product product;

	private Status status;
	private List<Integer> amountHistory;

	public Order(String orderId, String userId, Product product, Integer amount) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.product = product;
		this.status = Status.New;
		this.amountHistory = new ArrayList<>();
		this.amountHistory.add(amount);
	}

	public List<Integer> getAmountHistory() {
		return amountHistory;
	}

	public void setAmountHistory(List<Integer> amountHistory) {
		this.amountHistory = amountHistory;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void modify(Integer newAmount) {
		if (getAmount() < 0)
			amountHistory.remove(amountHistory.size() - 1);
		amountHistory.add(newAmount);
		status = Status.Modified;
	}

	public boolean cancel() {
		if (getAmount() < 0)
			return false;
		amountHistory.add(-1);
		status = Status.Canceled;
		return true;
	}

	public List<Integer> history() {
		return amountHistory;
	}

	public Integer getAmount() {
		if (amountHistory.size() == 0)
			return 0;
		Integer amount = amountHistory.get(amountHistory.size() - 1);
		if(amount < 0)
			return amountHistory.get(amountHistory.size() - 2) * amount;
		return amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public Product getProduct() {
		return product;
	}

	public String getUserId() {
		return userId;
	}

	public Status getStatus() {
		return status;
	}

	public boolean isCanceled() {
		return status == Status.Canceled;
	}

	public boolean isModified() {
		return amountHistory.size() > 2 || (amountHistory.size() > 1 && getAmount() >= 0);
	}
}
