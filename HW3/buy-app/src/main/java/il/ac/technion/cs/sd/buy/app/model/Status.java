package il.ac.technion.cs.sd.buy.app.model;


public enum Status {
	New(1), Modified(2), Canceled(3);
	Integer val;

	Status(Integer i) {
		val = i;
	}

	public Integer get() {
		return val;
	}
}