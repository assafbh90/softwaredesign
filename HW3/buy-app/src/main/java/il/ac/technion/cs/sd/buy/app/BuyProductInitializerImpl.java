package il.ac.technion.cs.sd.buy.app;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.inject.Inject;

import il.ac.technion.cs.sd.buy.app.model.Order;
import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.buy.app.model.User;
import il.ac.technion.cs.sd.buy.app.serializers.OrderSerializer;
import il.ac.technion.cs.sd.buy.app.serializers.ProductOrdersSerializer;
import il.ac.technion.cs.sd.buy.app.serializers.ProductUsersNumOfPurchasesSerializer;
import il.ac.technion.cs.sd.buy.app.serializers.ProductUsersSerializer;
import il.ac.technion.cs.sd.buy.app.serializers.UserProductsNumOfPurchasesSerializer;
import il.ac.technion.cs.sd.buy.app.serializers.UsersOrderSerializer;
import il.ac.technion.cs.sd.lib.LibraryManager;

public class BuyProductInitializerImpl implements BuyProductInitializer {
	private static final String ORDERS = "Orders";
	private static final String PRODUCT_ORDERS = "ProductOrders";
	private static final String PRODUCT_USERS_NUM_OF_PURCHASES = "ProductUsersNumOfPurchases";
	private static final String PRODUCT_USERS = "ProductUsers";
	private static final String USER_PRODUCTS_NUM_OF_PURCHASES = "UserProductsNumOfPurchases";
	private static final String USERS_ORDER = "UsersOrder";
	LibraryManager lib;

	@Inject
	public BuyProductInitializerImpl(LibraryManager lib) {
		this.lib = lib;
	}

	@Override
	public CompletableFuture<Void> setupXml(String xmlData) throws Exception {
		Map<String, User> users = new HashMap<>();
		Map<String, Product> products = new HashMap<>();
		Map<String, Order> orders = new HashMap<>();

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		Document doc = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new InputSource(new StringReader(xmlData)));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		doc.getDocumentElement().normalize();

		NodeList Products = doc.getElementsByTagName("Product");
		for (int i = 0; i < Products.getLength(); i++) {
			Node Product = Products.item(i);
			if (Product.getNodeType() == Node.ELEMENT_NODE) {
				Element ProductElement = (Element) Product;
				String productId = ProductElement.getElementsByTagName("id").item(0).getTextContent();
				String productPrice = ProductElement.getElementsByTagName("price").item(0).getTextContent();
				products.put(productId, new Product(productId, Integer.valueOf(productPrice)));
			}
		}

		NodeList Orders = doc.getChildNodes().item(0).getChildNodes();
		for (int i = 0; i < Orders.getLength(); i++) {
			Node Order = Orders.item(i);
			if (Order.getNodeType() == Node.ELEMENT_NODE) {
				Element OrderElement = (Element) Order;

				switch (Order.getNodeName()) {
				case "Order":
					String userId = OrderElement.getElementsByTagName("user-id").item(0).getTextContent();
					String orderId = OrderElement.getElementsByTagName("order-id").item(0).getTextContent();
					String productId = OrderElement.getElementsByTagName("product-id").item(0).getTextContent();
					String amount = OrderElement.getElementsByTagName("amount").item(0).getTextContent();

					orders.remove(orderId);
					if (!products.containsKey(productId))
						break;

					Order order = new Order(orderId, userId, products.get(productId), Integer.valueOf(amount));
					orders.put(orderId, order);

					break;
				case "ModifyOrder":
					String modifyOrderId = OrderElement.getElementsByTagName("order-id").item(0).getTextContent();
					String newAmount = OrderElement.getElementsByTagName("new-amount").item(0).getTextContent();

					if (!orders.containsKey(modifyOrderId))
						break;

					Order modifyOrder = orders.get(modifyOrderId);
					modifyOrder.modify(Integer.valueOf(newAmount));
					orders.put(modifyOrder.getOrderId(), modifyOrder);
					break;
				case "CancelOrder":
					String cancelOrderId = OrderElement.getElementsByTagName("order-id").item(0).getTextContent();

					if (!orders.containsKey(cancelOrderId))
						break;

					Order cancelOrder = orders.get(cancelOrderId);
					if (cancelOrder.cancel()) {
						orders.put(cancelOrder.getOrderId(), cancelOrder);
					}
					break;
				default:
					break;
				}
			}
		}

		CreateUsersAndProductsFromOrders(users, products, orders);

		BuildDatabase(users, products, orders);

		return CompletableFuture.completedFuture(null);
	}

	@Override
	public CompletableFuture<Void> setupJson(String jsonData) {
		try {
			Map<String, User> users = new HashMap<>();
			Map<String, Product> products = new HashMap<>();
			Map<String, Order> orders = new HashMap<>();
			
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(jsonData);
			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				if (jsonObject.get("type").equals("product")) {
					String productId = jsonObject.get("id").toString();
					String productPrice = jsonObject.get("price").toString();
					products.put(productId, new Product(productId, Integer.valueOf(productPrice)));
				}
			}

			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				
				switch (jsonObject.get("type").toString()) {
				case "order":
					String orderId = jsonObject.get("order-id").toString();
					String userId = jsonObject.get("user-id").toString();
					String productId = jsonObject.get("product-id").toString();
					String amount = jsonObject.get("amount").toString();
					
					orders.remove(orderId);
					if (!products.containsKey(productId))
						break;

					Order order = new Order(orderId, userId, products.get(productId), Integer.valueOf(amount));
					orders.put(orderId, order);

					break;
				case "modify-order":
					String modifyOrderId = jsonObject.get("order-id").toString();
					String newAmount = jsonObject.get("amount").toString();
					
					if (!orders.containsKey(modifyOrderId))
						break;

					Order modifyOrder = orders.get(modifyOrderId);
					modifyOrder.modify(Integer.valueOf(newAmount));
					orders.put(modifyOrder.getOrderId(), modifyOrder);
					break;
				case "cancel-order":
					String cancelOrderId = jsonObject.get("order-id").toString();

					if (!orders.containsKey(cancelOrderId))
						break;

					Order cancelOrder = orders.get(cancelOrderId);
					if (cancelOrder.cancel()) {
						orders.put(cancelOrder.getOrderId(), cancelOrder);
					}
					break;
				default:
					break;
				}
			}
	
			CreateUsersAndProductsFromOrders(users, products, orders);
			
			BuildDatabase(users, products, orders);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CompletableFuture.completedFuture(null);
	}

	private void CreateUsersAndProductsFromOrders(Map<String, User> users, Map<String, Product> products,
			Map<String, Order> orders) {
		for (Order order : orders.values()) {
			String userId = order.getUserId();
			User user = users.containsKey(userId) ? users.get(userId) : new User(userId);
			String productId = order.getProduct().getProductId();
			Product product = products.get(productId);
			product.addOrder(order);
			products.put(productId, product);
			user.order(order);
			users.put(userId, user);
		}
	}

	private void BuildDatabase(Map<String, User> users, Map<String, Product> products, Map<String, Order> orders)
			throws Exception {
		lib.setDatabase(lib.Builder()
				.addTable(ORDERS, new OrderSerializer(), orders.values().stream().collect(Collectors.toList()))
				.addTable(PRODUCT_ORDERS, new ProductOrdersSerializer(),
						products.values().stream().collect(Collectors.toList()))
				.addTable(PRODUCT_USERS_NUM_OF_PURCHASES, new ProductUsersNumOfPurchasesSerializer(),
						products.values().stream().collect(Collectors.toList()))
				.addTable(PRODUCT_USERS, new ProductUsersSerializer(),
						products.values().stream().collect(Collectors.toList()))
				.addTable(USER_PRODUCTS_NUM_OF_PURCHASES, new UserProductsNumOfPurchasesSerializer(),
						users.values().stream().collect(Collectors.toList()))
				.addTable(USERS_ORDER, new UsersOrderSerializer(), users.values().stream().collect(Collectors.toList()))
				.Build());
	}
}
