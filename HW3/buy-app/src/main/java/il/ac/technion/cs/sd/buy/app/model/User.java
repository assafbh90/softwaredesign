package il.ac.technion.cs.sd.buy.app.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
	private String userId;
	private Long amountOfMoney;
	private Integer canceledOrders;
	private Integer modifiedOrders;
	private Integer totalOrders;
	private List<String> orderIds;
	Map<String, Long> productIdsPurchase = new HashMap<String, Long>();
	
	public User(String userId) {
		this.userId = userId;
		this.amountOfMoney = Long.valueOf(0);
		this.canceledOrders = 0;
		this.modifiedOrders = 0;
		this.totalOrders = 0;
		this.orderIds = new ArrayList<>();
	}
	
	public void order(Order o) {
		if(o.getAmount() >= 0) {
			amountOfMoney += o.getAmount() * o.getProduct().getPrice();
			String productId = o.getProduct().getProductId();
			if(productIdsPurchase.containsKey(productId)) {
				productIdsPurchase.put(productId, productIdsPurchase.get(productId) + o.getAmount());
			} else {
				productIdsPurchase.put(productId, Long.valueOf(o.getAmount()));
			}
		}
		totalOrders++;
		switch (o.getStatus()) {
		case Canceled: 
			canceledOrders++;
			break;
		case Modified:
			modifiedOrders++;
		default:
			break;
		}
		orderIds.add(o.getOrderId());
	}

	public Long getAmountOfMoney() {
		return amountOfMoney;
	}

	public void setAmountOfMoney(Long amountOfMoney) {
		this.amountOfMoney = amountOfMoney;
	}

	public Integer getCanceledOrders() {
		return canceledOrders;
	}

	public void setCanceledOrders(Integer canceledOrders) {
		this.canceledOrders = canceledOrders;
	}

	public Integer getModifiedOrders() {
		return modifiedOrders;
	}

	public void setModifiedOrders(Integer modifiedOrders) {
		this.modifiedOrders = modifiedOrders;
	}

	public Integer getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}

	public Map<String, Long> getProductIdsPurchase() {
		return productIdsPurchase;
	}

	public void setProductIdsPurchase(Map<String, Long> productIdsPurchase) {
		this.productIdsPurchase = productIdsPurchase;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setOrderIds(List<String> orderIds) {
		this.orderIds = orderIds;
	}
	
	public String getUserId() {
		return userId;
	}

	public List<String> getOrderIds() {
		return orderIds;
	}

}
