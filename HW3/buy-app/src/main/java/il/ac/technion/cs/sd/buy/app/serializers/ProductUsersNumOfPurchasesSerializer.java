package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

// ProductId -> map(UserId , NumOfPurchases)
public class ProductUsersNumOfPurchasesSerializer  implements DatabaseSerializer<Product> {

	@Override
	public Product objectFactory(String key, List<String> value) {
		Product product = new Product(key, -1);
		product.setUserIdsPurchase(value.stream().collect(Collectors.toMap(x->x.split(",")[0], x->Long.valueOf(x.split(",")[1]))));
		return product;
	}

	@Override
	public String getKey(Product object) {
		return object.getProductId();
	}

	@Override
	public List<String> getValue(Product object) {
		return object.getUserIdsPurchase()
				.entrySet()
				.stream()
				.map(e->e.getKey() + "," + e.getValue())
				.collect(Collectors.toList());
	}

}
