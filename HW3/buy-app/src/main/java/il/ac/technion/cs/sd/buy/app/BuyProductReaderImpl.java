package il.ac.technion.cs.sd.buy.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.google.inject.Inject;

import il.ac.technion.cs.sd.buy.app.model.Order;
import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.buy.app.model.User;
import il.ac.technion.cs.sd.lib.Database;
import il.ac.technion.cs.sd.lib.Database.TableNotFound;
import il.ac.technion.cs.sd.lib.LibraryManager;

public class BuyProductReaderImpl implements BuyProductReader{
	private static final String ORDERS = "Orders";
	private static final String PRODUCT_ORDERS = "ProductOrders";
	private static final String PRODUCT_USERS_NUM_OF_PURCHASES = "ProductUsersNumOfPurchases";
	private static final String PRODUCT_USERS = "ProductUsers";
	private static final String USER_PRODUCTS_NUM_OF_PURCHASES = "UserProductsNumOfPurchases";
	private static final String USERS_ORDER = "UsersOrder";
	private Database db;

	@Inject
	private BuyProductReaderImpl(LibraryManager lib) {
		Optional<Database> optional = lib.getDatabase();
		if (optional.isPresent()) {
			db = optional.get();
		} else {
			throw new Error("Database not present yet");
		}
	}
	
	@Override
	public CompletableFuture<Boolean> isValidOrderId(String orderId) throws TableNotFound {
		return db.from(ORDERS).thenCompose(v -> v.get(orderId)).thenApply(v->v.isPresent());
	}

	@Override
	public CompletableFuture<Boolean> isCanceledOrder(String orderId) throws TableNotFound {
		return db.from(ORDERS).thenCompose(v -> v.get(orderId)).thenApply(v->v.isPresent()?((Order)v.get()).isCanceled():false);
	}

	@Override
	public CompletableFuture<Boolean> isModifiedOrder(String orderId) throws TableNotFound {
		return db.from(ORDERS).thenCompose(v -> v.get(orderId)).thenApply(v->v.isPresent()?((Order)v.get()).isModified():false);
	}

	@Override
	public CompletableFuture<OptionalInt> getNumberOfProductOrdered(String orderId) throws TableNotFound {
		return db.from(ORDERS).thenCompose(v -> v.get(orderId)).thenApply(v->v.isPresent()?OptionalInt.of(((Order)v.get()).getAmount()):OptionalInt.empty());
	}

	@Override
	public CompletableFuture<List<Integer>> getHistoryOfOrder(String orderId) throws TableNotFound {
		return db.from(ORDERS).thenCompose(v -> v.get(orderId)).thenApply(v->v.isPresent()?((Order)v.get()).getAmountHistory():new ArrayList<Integer>());
	}

	@Override
	public CompletableFuture<List<String>> getOrderIdsForUser(String userId) throws TableNotFound {
		return db.from(USERS_ORDER).thenCompose(v -> v.get(userId)).thenApply(v->v.isPresent()?((User)v.get()).getOrderIds().stream().sorted().collect(Collectors.toList()):new ArrayList<String>());
	}

	@Override
	public CompletableFuture<Long> getTotalAmountSpentByUser(String userId) throws TableNotFound {
		return db.from(USERS_ORDER).thenCompose(v -> v.get(userId)).thenApply(v->v.isPresent()?((User)v.get()).getAmountOfMoney():0);
	}
	
	private List<String> getUserIdsPurchaseListOfProduct(Optional<Object> v) {
		return ((Product)v.get()).getUserIdsPurchase().keySet().stream().collect(Collectors.toList());
	}

	@Override
	public CompletableFuture<List<String>> getUsersThatPurchased(String productId) throws TableNotFound {
		return db.from(PRODUCT_USERS).thenCompose(v -> v.get(productId)).thenApply(v->v.isPresent()?getUserIdsPurchaseListOfProduct(v):new ArrayList<String>());
	}

	@Override
	public CompletableFuture<List<String>> getOrderIdsThatPurchased(String productId) throws TableNotFound {
		return db.from(PRODUCT_ORDERS).thenCompose(v -> v.get(productId)).thenApply(v->v.isPresent()?((Product)v.get()).getOrderIds().stream().sorted().collect(Collectors.toList()):new ArrayList<String>());
	}

	@Override
	public CompletableFuture<OptionalLong> getTotalNumberOfItemsPurchased(String productId) throws TableNotFound {
		return db.from(PRODUCT_USERS).thenCompose(v -> v.get(productId)).thenApply(v->v.isPresent()?OptionalLong.of(((Product)v.get()).getPurchasedAmount()):OptionalLong.empty());
	}

	@Override
	public CompletableFuture<OptionalDouble> getAverageNumberOfItemsPurchased(String productId) throws TableNotFound {
		return db.from(PRODUCT_USERS).thenCompose(v -> v.get(productId)).thenApply(v->{
			if(v.isPresent()){
				Product product = ((Product)v.get());
				Double purchasedAmount = Double.valueOf(product.getPurchasedAmount());
				Integer numOfBuyers = product.getUserIdsPurchase().keySet().size();
				if(numOfBuyers == 0) {
					return OptionalDouble.empty();
				}
				return OptionalDouble.of(purchasedAmount/numOfBuyers);
			} 
			return OptionalDouble.empty();
		});
	}

	@Override
	public CompletableFuture<OptionalDouble> getCancelRatioForUser(String userId) throws TableNotFound {
		return db.from(USERS_ORDER).thenCompose(v -> v.get(userId)).thenApply(v->{
			if(v.isPresent()){
				User user = ((User)v.get());
				return OptionalDouble.of(Double.valueOf(user.getCanceledOrders())/Double.valueOf(user.getTotalOrders()));
			} 
			return OptionalDouble.empty();
		});
	}

	@Override
	public CompletableFuture<OptionalDouble> getModifyRatioForUser(String userId) throws TableNotFound {
		return db.from(USERS_ORDER).thenCompose(v -> v.get(userId)).thenApply(v->{
			if(v.isPresent()){
				User user = ((User)v.get());
				return OptionalDouble.of(Double.valueOf(user.getModifiedOrders())/Double.valueOf(user.getTotalOrders()));
			} 
			return OptionalDouble.empty();
		});	}

	@Override
	public CompletableFuture<Map<String, Long>> getAllItemsPurchased(String userId) throws TableNotFound {
		return db.from(USER_PRODUCTS_NUM_OF_PURCHASES).thenCompose(v -> v.get(userId)).thenApply(v->v.isPresent()?((User)v.get()).getProductIdsPurchase():new HashMap<>());
	}

	@Override
	public CompletableFuture<Map<String, Long>> getItemsPurchasedByUsers(String productId) throws TableNotFound {
		return db.from(PRODUCT_USERS_NUM_OF_PURCHASES).thenCompose(v -> v.get(productId)).thenApply(v->v.isPresent()?((Product)v.get()).getUserIdsPurchase():new HashMap<>());
	}
}
