package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

// ProductId -> PurchasedItemsAmount(long)
// ProductId -> NumOfBuyers <- just counting
// ProductId -> List<UserId>
public class ProductUsersSerializer implements DatabaseSerializer<Product>{

	@Override
	public Product objectFactory(String key, List<String> value) {
		Product product = new Product(key, -1);
		product.setPurchasedAmount(Integer.valueOf(value.get(0)));
		product.setUserIdsPurchase(value.stream().skip(1).collect(Collectors.toMap(x->x, x->Long.valueOf(0))));
		return product;
	}

	@Override
	public String getKey(Product object) {
		return object.getProductId();
	}

	@Override
	public List<String> getValue(Product object) {
		List<String> list = new ArrayList<>();
		list.add("" + object.getPurchasedAmount());
		list.addAll(object.getUserIdsPurchase()
				.keySet()
				.stream()
				.collect(Collectors.toList()));
		return list;
	}

}
