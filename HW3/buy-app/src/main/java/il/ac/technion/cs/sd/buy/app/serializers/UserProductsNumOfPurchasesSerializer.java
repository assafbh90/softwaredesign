package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.buy.app.model.User;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

// UserId -> map(ProductId , NumOfPurchases)
public class UserProductsNumOfPurchasesSerializer implements DatabaseSerializer<User> {

	@Override
	public User objectFactory(String key, List<String> value) {
		User user = new User(key);
		user.setProductIdsPurchase(value.stream().collect(Collectors.toMap(x->x.split(",")[0], x->Long.valueOf(x.split(",")[1]))));
		return user;
	}

	@Override
	public String getKey(User object) {
		return object.getUserId();
	}

	@Override
	public List<String> getValue(User object) {
		return object.getProductIdsPurchase()
				.entrySet()
				.stream()
				.map(e->e.getKey() + "," + e.getValue())
				.collect(Collectors.toList());
	}

}
