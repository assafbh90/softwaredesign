package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.buy.app.model.User;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

// UserId ->   amountOfMoney,
// UserId ->   numOfCancelled,
// UserId ->   numOfmodified,
// UserId ->   totalOrders,
// UserId ->   List<OrderId>
public class UsersOrderSerializer implements DatabaseSerializer<User> {

	@Override
	public User objectFactory(String key, List<String> value) {
		User user = new User(key);
		user.setAmountOfMoney(Long.valueOf(value.get(0)));
		user.setCanceledOrders(Integer.valueOf(value.get(1)));
		user.setModifiedOrders(Integer.valueOf(value.get(2)));
		user.setTotalOrders(Integer.valueOf(value.get(3)));
		user.setOrderIds(value.stream().skip(4).collect(Collectors.toList()));
		return user;
	}

	@Override
	public String getKey(User object) {
		return object.getUserId();
	}

	@Override
	public List<String> getValue(User object) {
		List<String> list = new ArrayList<>();
		list.add("" + object.getAmountOfMoney());
		list.add("" + object.getCanceledOrders());
		list.add("" + object.getModifiedOrders());
		list.add("" + object.getTotalOrders());
		list.addAll(object.getOrderIds());
		return list;
	}

}
