package il.ac.technion.cs.sd.buy.app.serializers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import il.ac.technion.cs.sd.buy.app.model.Order;
import il.ac.technion.cs.sd.buy.app.model.Product;
import il.ac.technion.cs.sd.buy.app.model.Status;
import il.ac.technion.cs.sd.lib.DatabaseSerializer;

//    OrderId ->  ProductId
//    OrderId ->  Status
//    OrderId ->  History
public class OrderSerializer implements DatabaseSerializer<Order> {

	@Override
	public Order objectFactory(String key, List<String> value) {
		String productId = value.get(0);
		String status = value.get(1);
		List<Integer> history = value.stream().skip(2).map(v->Integer.valueOf(v)).collect(Collectors.toList());
		Order order = new Order(key,"-", new Product(productId, -1),0);
		order.setAmountHistory(history);
		order.setStatus(Status.valueOf(status));
		return order;
	}

	@Override
	public String getKey(Order object) {
		return object.getOrderId();
	}

	@Override
	public List<String> getValue(Order object) {
		List<String> list = new ArrayList<>();
		list.add(object.getProduct().getProductId());
		list.add(object.getStatus().name());
		list.addAll(object.getAmountHistory().stream().map(v->"" + v).collect(Collectors.toList()));
		return list;
	}

}
