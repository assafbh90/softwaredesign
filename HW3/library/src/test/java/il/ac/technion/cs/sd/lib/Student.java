package il.ac.technion.cs.sd.lib;

public class Student{
	
	private static final String commaSeperator = ",";
	private static final int ID = 0;
	private static final int GRADE = 1;
	
	private final String id;
	private final Integer grade;
	
	public Student(String id, Integer grade) {
		Integer.parseInt(id);
		this.id = id;
		this.grade = grade;
	}
	public Student(String csvStudent) {
		if (csvStudent == "") {
			this.id = "0";
			this.grade = 0;
			return;
		}

		String[] studentFields = csvStudent.split(commaSeperator);
		Integer.parseInt(studentFields[ID]);
		this.id = studentFields[ID];
		this.grade = Integer.parseInt(studentFields[GRADE]);
	}
	
	public String getId() {
		return id;
	}
	
	public Integer getGrade() {
		return grade;
	}
	
	public String toString() {
		return getId() + "," + getGrade();
	}
	
}
