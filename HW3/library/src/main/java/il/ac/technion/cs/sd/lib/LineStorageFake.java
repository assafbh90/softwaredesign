package il.ac.technion.cs.sd.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import il.ac.technion.cs.sd.buy.ext.FutureLineStorage;

public class LineStorageFake implements FutureLineStorage {
	List<String> storage = Collections.synchronizedList(new ArrayList<String>());

	@Override
	public CompletableFuture<Void> appendLine(String s) {
		return CompletableFuture.supplyAsync(() -> {
			storage.add(s);
			return null;
		});
	}

	@Override
	public CompletableFuture<String> read(int lineNumber) {
		return CompletableFuture.supplyAsync(() -> {
			String s = storage.get(lineNumber);
			try {
				Thread.sleep(s.length());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return s;
		});
	}

	@Override
	public CompletableFuture<Integer> numberOfLines() {
		return CompletableFuture.supplyAsync(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return storage.size();
		});
	}

}
