package il.ac.technion.cs.sd.lib;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TableDefault<T> implements Table<T> {

	private DatabaseSerializer<T> ser;
	private Storage storage;
	private Parser<T> parser;
	private CompletableFuture<Integer> size;

	TableDefault(DatabaseSerializer<T> ser, Storage storage, Parser<T> parser) {
		this.ser = ser;
		this.storage = storage;
		this.parser = parser;
		this.size = CompletableFuture.completedFuture(0);
	}

	@Override
	public CompletableFuture<Table<T>> add(List<T> objects) {
		Collections.reverse(objects);
		return addListToTable(objects.stream().filter(distinctByKey(ser::getKey))
				.sorted(Comparator.comparing(ser::getKey)).collect(Collectors.toList()))
						.thenRun(() -> this.size = storage.numberOfLines()).thenApply(v -> this);
	}

	@Override
	public CompletableFuture<Optional<T>> get(String key) {
		return size.thenCompose(s -> binarySearch(0, s, key));
	}

	private CompletableFuture<Optional<T>> binarySearch(int start, int end, String key) {
		if (start < end) {
			int mid = start + (end - start) / 2;
			return storage.read(mid).thenCompose(k -> {
				if (key.compareTo(k) < 0) {
					return binarySearch(start, mid, key);
				} else if (key.compareTo(k) > 0) {
					return binarySearch(mid + 1, end, key);
				} else {
					return storage.readValue(mid).thenCompose(v -> {
						List<String> valueFromDB = parser.getValue(v).stream().map(s -> unescape(s))
								.collect(Collectors.toList());
						return CompletableFuture
								.supplyAsync(() -> Optional.ofNullable(ser.objectFactory(k, valueFromDB)));
					});
				}
			});
		}
		return CompletableFuture.supplyAsync(() -> Optional.empty());
	}

	@Override
	public CompletableFuture<Integer> size() {
		return size;
	}

	private CompletableFuture<Void> addListToTable(List<T> objects) {
		CompletableFuture<Void> comp = CompletableFuture.completedFuture(null);

		for (T o : objects) {
			String key = escape(ser.getKey(o));
			List<String> values = ser.getValue(o).stream().map(s -> escape(s)).collect(Collectors.toList());
			comp = comp.thenCompose(v -> storage.appendLine(parser.toCsv(key, values)));
		}

		return comp;
	}

	private <R> Predicate<R> distinctByKey(Function<? super R, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> !Optional.ofNullable(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE)).isPresent();
	}

	private final char EscapeChar = 14;

	private String escape(String s) {
		return s.replace(',', EscapeChar);
	}

	private String unescape(String s) {
		return s.replace(EscapeChar, ',');
	}
}

interface TableFactory {
	<T> CompletableFuture<Table<T>> create(DatabaseSerializer<T> ser, List<T> objects, Storage storage,
			Parser<T> parser) throws Exception;
}

class TableFactoryImpl implements TableFactory {
	@Override
	public <T> CompletableFuture<Table<T>> create(DatabaseSerializer<T> ser, List<T> objects, Storage storage,
			Parser<T> parser) throws Exception {
		Table<T> table = new TableDefault<T>(ser, storage, parser);
		DatabaseModule.getInjector().injectMembers(table);
		return table.add(objects);
	}
}