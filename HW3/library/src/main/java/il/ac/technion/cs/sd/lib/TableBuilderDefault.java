package il.ac.technion.cs.sd.lib;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.google.inject.Inject;

class TableBuilderDefault<T> implements TableBuilder<T> {
	final private Map<String, CompletableFuture<Table<?>>> tables;
	final private String name;
	final private DatabaseSerializer<T> serializer;
	@Inject
	private DatabaseBuilderFactory dbBuilderFactory;
	@Inject
	private TableFactory tableFactory;
	@Inject
	private StorageFactory storageFactory;

	TableBuilderDefault(Map<String, CompletableFuture<Table<?>>> tables, String name, DatabaseSerializer<T> serializer) {
		this.tables = tables;
		this.name = name;
		this.serializer = serializer;
	}

	@Override
	public DatabaseBuilder add(List<T> objects) throws Exception {
		Parser<T> parser = new Parser<>(serializer);
		Storage storage = storageFactory.create(parser);
		CompletableFuture<Table<T>> table = tableFactory.create(serializer, objects, storage, parser);
		tables.put(name, table.thenApply(v->(Table<?>)v));
		return dbBuilderFactory.create(tables);
	}
}

interface TableBuilderFactory {
	<T> TableBuilder<T> create(Map<String, CompletableFuture<Table<?>>> tables, String name, DatabaseSerializer<T> serializer);
}

class TableBuilderFactoryImpl implements TableBuilderFactory {
	@Override
	public <T> TableBuilder<T> create(Map<String, CompletableFuture<Table<?>>> tables, String name, DatabaseSerializer<T> serializer) {
		TableBuilder<T> tableBuilder = new TableBuilderDefault<T>(tables, name, serializer);
		DatabaseModule.getInjector().injectMembers(tableBuilder);
		return tableBuilder;
	}
}