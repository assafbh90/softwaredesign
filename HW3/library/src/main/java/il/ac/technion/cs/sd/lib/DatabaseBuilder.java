package il.ac.technion.cs.sd.lib;

import java.util.List;

public interface DatabaseBuilder {
	<T> TableBuilder<T> addTable(String name, DatabaseSerializer<T> serializer);

	<T> DatabaseBuilder addTable(String name, DatabaseSerializer<T> serializer, List<T> objects) throws Exception;

	Database Build() throws Exception;
}
