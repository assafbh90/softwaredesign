package il.ac.technion.cs.sd.lib;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Parser<T> {
	private static final String DBSeparator = ",";
	private final static String newLineSeparator = "\n";
	DatabaseSerializer<T> ser;

	private List<String> splitFromCharacter(String string, String separator) {
		return Stream.of(string)
				.map(subString -> subString.split(separator))
				.flatMap(Arrays::stream)
				.collect(Collectors.toList());
	}
	
	public Parser(DatabaseSerializer<T> ser) {
		this.ser = ser;
	}

	public String getKey(String result) {
		return result.split(DBSeparator)[0];
	}

	public List<String> getValue(String result) {
		return Stream.of(result.split(DBSeparator)).skip(1).collect(Collectors.toList());
	}

	public String toCsv(List<String> value) {
		return value.stream().reduce("", (a, b) -> a + DBSeparator + b);
	}

	public String toCsv(String key, List<String> value) {
		return value.stream().reduce(key, (a, b) -> a + DBSeparator + b);
	}

	public List<String> parseToLines(String stringToSplit) {
		if (stringToSplit == "")
			return Arrays.asList();
		return splitFromCharacter(stringToSplit, newLineSeparator);
	}

	public List<T> Parse(String string) {
		if (string == "")
			return Arrays.asList();
		string = string.replaceAll("\r", "");
		return parseToLines(string).stream().map(subString -> ser.objectFactory(getKey(subString), getValue(subString)))
				.collect(Collectors.toList());
	}
}
