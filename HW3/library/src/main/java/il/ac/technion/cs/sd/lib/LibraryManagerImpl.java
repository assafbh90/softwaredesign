package il.ac.technion.cs.sd.lib;

import java.util.Optional;

import com.google.inject.Inject;

public class LibraryManagerImpl implements LibraryManager {
	private DatabaseBuilder builder;
	private Optional<Database> database;
	
	@Inject
	public LibraryManagerImpl(DatabaseBuilder builder) {
		this.builder = builder;
		this.database = Optional.empty();
	}
	
	@Override
	public DatabaseBuilder Builder() {
		return builder;
	}

	@Override
	public Optional<Database> getDatabase() {
		return database;
	}

	@Override
	public void setDatabase(Database database) {
		this.database = Optional.ofNullable(database);
	}

}
