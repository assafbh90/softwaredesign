package il.ac.technion.cs.sd.lib;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

public class DatabaseDefault implements Database {
	private Map<String, CompletableFuture<Table<?>>> tables;

	@Inject
	DatabaseDefault(@Assisted Map<String, CompletableFuture<Table<?>>> tables) throws Exception {
		this.tables = tables;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> CompletableFuture<Table<T>> from(String table) throws TableNotFound {
		Optional<CompletableFuture<Table<?>>> t = Optional.ofNullable(tables.get(table));
		if (!t.isPresent()) throw new TableNotFound();
		return t.get().thenApply(v->(Table<T>)v);
	}
}

interface DatabaseFactory {
	DatabaseDefault create(Map<String, CompletableFuture<Table<?>>> tables);
}