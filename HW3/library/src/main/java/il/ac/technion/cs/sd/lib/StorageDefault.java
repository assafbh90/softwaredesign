package il.ac.technion.cs.sd.lib;

import java.util.concurrent.CompletableFuture;
import com.google.inject.Inject;
import il.ac.technion.cs.sd.buy.ext.FutureLineStorage;

class StorageDefault<T> implements Storage {
	@Inject
	private CompletableFuture<FutureLineStorage> keyStorage;
	@Inject
	private CompletableFuture<FutureLineStorage> valueStorage;
	final private Parser<T> parser;

	StorageDefault(Parser<T> parser) {
		this.parser = parser;
	}

	@Override
	public CompletableFuture<Void> appendLine(String line) {
		return keyStorage.thenCompose(storage1 -> storage1.appendLine(parser.getKey(line)))
				.thenRun(()->valueStorage.thenCompose(storage2 -> storage2.appendLine(parser.toCsv(parser.getValue(line)))));
	}

	@Override
	public CompletableFuture<Integer> numberOfLines() {
		return keyStorage.thenCompose(v -> v.numberOfLines());
	}

	@Override
	public CompletableFuture<String> read(int line) {
		return keyStorage.thenCompose(v -> v.read(line));
	}

	@Override
	public CompletableFuture<String> readValue(int line) {
		return valueStorage.thenCompose(v -> v.read(line));
	}
}

interface StorageFactory {
	<T> Storage create(Parser<T> parser);
}

class StorageFactoryImpl implements StorageFactory {

	@Override
	public <T> Storage create(Parser<T> parser) {
		Storage storage = new StorageDefault<T>(parser);
		DatabaseModule.getInjector().injectMembers(storage);
		return storage;
	}

}