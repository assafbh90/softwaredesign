package il.ac.technion.cs.sd.lib;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface Table<T> {
	CompletableFuture<Table<T>> add(List<T> objects);
	
	CompletableFuture<Optional<T>> get(String key);

	 CompletableFuture<Integer> size();
}
