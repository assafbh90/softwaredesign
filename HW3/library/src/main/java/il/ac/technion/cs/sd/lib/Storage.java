package il.ac.technion.cs.sd.lib;

import java.util.concurrent.CompletableFuture;
import il.ac.technion.cs.sd.buy.ext.FutureLineStorage;

public interface Storage extends FutureLineStorage {
	public CompletableFuture<String> readValue(int line);
}
