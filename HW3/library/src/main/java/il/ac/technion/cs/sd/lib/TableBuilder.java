package il.ac.technion.cs.sd.lib;

import java.util.List;

public interface TableBuilder<T> {
	DatabaseBuilder add(List<T> objects) throws Exception;
}
