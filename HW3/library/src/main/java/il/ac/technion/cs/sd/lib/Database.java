package il.ac.technion.cs.sd.lib;

import java.util.concurrent.CompletableFuture;

public interface Database {
	@SuppressWarnings("serial")
	class TableNotFound extends Exception{};
	<T> CompletableFuture<Table<T>> from(String table) throws TableNotFound;
}
