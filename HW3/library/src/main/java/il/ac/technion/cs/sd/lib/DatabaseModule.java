package il.ac.technion.cs.sd.lib;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.assistedinject.FactoryModuleBuilder;

import il.ac.technion.cs.sd.buy.ext.FutureLineStorage;

public class DatabaseModule extends AbstractModule {
	static public Injector getInjector() {
		return Guice.createInjector(new DatabaseModule());
	}

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		FactoryModuleBuilder moduleBuilder = new FactoryModuleBuilder();
		install(moduleBuilder.implement(Database.class, DatabaseDefault.class).build(DatabaseFactory.class));

		bind(DatabaseBuilderFactory.class).to(DatabaseBuilderFactoryImpl.class);
		bind(TableBuilderFactory.class).to(TableBuilderFactoryImpl.class);
		bind(TableFactory.class).to(TableFactoryImpl.class);
		bind(StorageFactory.class).to(StorageFactoryImpl.class);
		bind(DatabaseBuilder.class).to(DatabaseBuilderDefault.class);
		bind(LibraryManager.class).to(LibraryManagerImpl.class);
	}

//	static private Integer fileNum = 0;

	@Provides
	CompletableFuture<FutureLineStorage> lineStorageProvider() throws InterruptedException, ExecutionException {
//		return Guice.createInjector(new LineStorageModule()).getInstance(FutureLineStorageFactory.class).open("" + fileNum++);
		Thread.sleep(100);
		return CompletableFuture.supplyAsync(()->new LineStorageFake());
	}
}
